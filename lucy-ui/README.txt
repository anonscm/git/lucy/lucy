*******************************************************************
* README for lucy user interface
*******************************************************************

#installation

lucyui.war: 

- Move the lucyui war file in your tomcat:
{HOME_DIRECTORY}/tomcat/webapp directory

- Run the tomcat server

- Try to type this line in your browser:
http://server:port/lucyui/org.evolvis.lucy.ui.LucySearchModul/lucy-index.html

for more information see the lucy wiki:
https://wiki.evolvis.org/lucy/index.php/Lucy-user_interface