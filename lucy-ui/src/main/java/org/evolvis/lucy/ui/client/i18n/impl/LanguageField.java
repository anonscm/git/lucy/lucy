/*
 * lucy-ui,
 * lucy user interface
 * Copyright (C) 2000-2009 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'lucy-ui'
 * Signature of Elmar Geese, 21 April 2009
 * Elmar Geese, CEO tarent GmbH.
 */

package org.evolvis.lucy.ui.client.i18n.impl;

import com.google.gwt.user.client.ui.ClickListener;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Widget;

/**
 * This class has an method to build an language field
 * for the lucy ui.
 * 
 * @author Viktor Hamm tarent GmbH Bonn
 *
 */
public class LanguageField {

	/**
	 * @author Frederic Eßer
	 * 
	 * This method display the Flags for the internationalization
	 * 
	 * @return languageField
	 */
	public HorizontalPanel buildLanguageField() 
	{
		// Create the languageField as a HorizontalPanel
		HorizontalPanel languageField = new HorizontalPanel();
		
		// Set the StyleName
		languageField.setStyleName("languageList");
		
		// Create images
		Image interDe = new Image("img/de.jpg");
		Image interEn = new Image("img/en.jpg");
		
		// Set the StylName for the image
		interDe.setStyleName("languageFlag");
		interEn.setStyleName("languageFlag");
		
		// Set title of the image
		interDe.setTitle(" Deutsch ");
		interEn.setTitle(" English ");
		
		languageField.add(interDe);
		languageField.add(interEn);
		
		interDe.addClickListener(new ClickListener()
		{
			/**
			 * @see com.google.gwt.user.client.ui.ClickListener#onClick(com.google.gwt.user.client.ui.Widget)
			 */
			public void onClick(Widget sender)
			{
				reloadDE();
			}
			
			// Add new languages here
		    /**
		     * @author Frederic Eßer
		     * 
		     * This function reload the page with German local parameter
		     * 
		     */
			public native void reloadDE() /*-{
				var target = "?locale=de";
				$wnd.location = target;
			}-*/;
		});
		
		interEn.addClickListener(new ClickListener() 
		{
			
			/**
			 * @see com.google.gwt.user.client.ui.ClickListener#onClick(com.google.gwt.user.client.ui.Widget)
			 */
			public void onClick(Widget sender)
			{
				reloadEN();
			}
			
		    /**
		     * @author Frederic Eßer
		     * 
		     * This function reload the page with English local parameter
		     * 
		     */
			public native void reloadEN() /*-{
				var target = "?locale=en";
				$wnd.location = target;
			}-*/;
		});

		return languageField;
	}
}
