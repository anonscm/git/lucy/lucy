/*
 * lucy-ui,
 * lucy user interface
 * Copyright (C) 2000-2009 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'lucy-ui'
 * Signature of Elmar Geese, 21 April 2009
 * Elmar Geese, CEO tarent GmbH.
 */

package org.evolvis.lucy.ui.client.ui;


import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;

import org.evolvis.lucy.ui.client.LucySearchModul;
import org.evolvis.lucy.ui.client.i18n.InternationalizationConstants;
import org.evolvis.lucy.ui.client.req.SearchResultHandler;

import com.google.gwt.core.client.GWT;
import com.google.gwt.http.client.RequestBuilder;
import com.google.gwt.http.client.RequestException;
import com.google.gwt.json.client.JSONArray;
import com.google.gwt.json.client.JSONException;
import com.google.gwt.json.client.JSONObject;
import com.google.gwt.json.client.JSONString;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.ClickListener;
import com.google.gwt.user.client.ui.DisclosurePanel;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.Hidden;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.TabPanel;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;


/**
 * This class parse and show search results in the ui
 * 
 * @author Viktor Hamm tarent GmbH Bonn
 */
public class Content {

	static InternationalizationConstants constants = (InternationalizationConstants) GWT
	.create(InternationalizationConstants.class);
	
	static Input inputfield = new Input();
	static UIComponents uicomponents = new UIComponents();
	
	Image textIcon = new Image("img/icons/tango/mimetypes/text-x-generic.png");
	Image picIcon = new Image("img/icons/tango/mimetypes/image-x-generic.png");
	
	/**
	 * Map with system icons
	 */
	Map<String, String> icon = new LinkedHashMap<String, String>();
	
	public static HTML searchResultHeadText = new HTML();
	public static HTML lucySearchForText = new HTML();
	
	/**
	 * variable for MultiWordSuggestOracle list
	 * @see com.google.gwt.user.client.ui.MultiWordSuggestOracle
	 */
	public static Hidden oracle_content;
	public static String searchText;
	public static Hidden user_input;
	
	/**
	 * Panels to input the search results
	 */
	public static TabPanel resultTab;
	public static VerticalPanel infra;
	public static VerticalPanel intra;
	public static VerticalPanel evo;
	public static VerticalPanel tc;
	
	public static VerticalPanel infra_tmp;
	public static VerticalPanel intra_tmp;
	public static VerticalPanel evo_tmp;
	public static VerticalPanel tc_tmp;
	
	public String tcid = "off";
	public String intraid = "off";
	public String infraid = "off";
	public String evoid = "off";

	// show result count in tab
	public static int evoCount;
	public static int infraCount;
	public static int intraCount;
	public static int tcCount;
	
	public String data = null;
	public int datacount = 0;
	
	//WikiParser parser = new WikiParser();
	//Map<String,String> tmp = new LinkedHashMap<String, String>();
	
	/**
	 * the static header text for the search result field
	 * 
	 * @return HTML resultHeadText
	 */
	public HTML buildHeaderText(int resultCount) {
		searchResultHeadText.setHTML("<span class=\"searchResultsHead\">" + constants.best_hits()
				+ "</span><br><span class=\"resultCount\">" + resultCount + " " + constants.ergFound() + "</span>");
		searchResultHeadText.setVisible(true);
		searchResultHeadText.setStyleName("result-headline");
		return searchResultHeadText;
	}
	
	/**
	 * This method send an search request to the {@link LucySearchService} servlet
	 * 
	 * @author mhelle
	 */
	public static void search(){
		
		JSONObject o = new JSONObject();
		RequestBuilder rb = null;
		user_input.setValue(inputfield.input.getText());
		try {
			o.put("endpoint", new JSONString(LucySearchModul.getTargetAddress()));
			o.put("q", new JSONString(inputfield.input.getText().toString()));
			o.put("mode", new JSONString("search"));
			
			JSONArray systems = new JSONArray();
			for(int i = 0;i<uicomponents.activated_system_list.size();i++){
				systems.set(i, new JSONString(uicomponents.activated_system_list.get(i)));
			}
			o.put("systems", systems);
//			if(Input.searchtext == null) {
//				o.put("q", new JSONString(inputfield.input.getText().toString()));
//			}else {
//				o.put("q", new JSONString(Input.searchtext.toString()));
//			}
			for(int i = 0;i < uicomponents.SYSTEM_TYPES.length; i++){
				for(int e = 0;e < uicomponents.activated_system_list.size(); e++){
					if(uicomponents.SYSTEM_TYPES[i] == uicomponents.activated_system_list.get(e) || uicomponents.SYSTEM_TYPES[i].equals(uicomponents.activated_system_list.get(e))){
						o.put(uicomponents.SYSTEM_TYPES[i],new JSONString("on"));
						break;
					} else {
						o.put(uicomponents.SYSTEM_TYPES[i],new JSONString("off"));
					}
				}
			}
			rb = new RequestBuilder(RequestBuilder.POST, GWT.getModuleBaseURL() + "/LucySearchService");
			try {
				rb.sendRequest(o.toString(), new SearchResultHandler());
			} catch (RequestException e) {
				e.printStackTrace();
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * This method create an Hidden field to save temporary the suggest list 
	 * of the SuggestOracle
	 *  
	 * @return Hidden oracle_content;
	 */
	public Hidden oracleHiddenField() {
		oracle_content = new Hidden();
		return oracle_content;
	}
	
	/**
	 * This method saves the user input search word
	 * 
	 * @return
	 */
	public Hidden userInputHiddenField() {
		user_input = new Hidden();
		return user_input;
	}
	
	/**
	 * This Method build the search room text with an link to be set the extendet
	 * search field on visible
	 * 
	 * @return VerticalPanel panel
	 */
	public VerticalPanel buildSearchRoomTextField() {
		RootPanel.get("checkboxfield").add(uicomponents.generateSystems());
		
		VerticalPanel panel = new VerticalPanel();

		HTML expand = new HTML("<a>" + constants.expand_area() + "</a></p>");
		expand.setStyleName("expandarea");
		
		if (uicomponents.systemCheckBox.isVisible())
			uicomponents.systemCheckBox.setVisible(false);

		expand.addClickListener(new ClickListener() {

			/**
			 * @see com.google.gwt.user.client.ui.ClickListener#onClick(com.google.gwt.user.client.ui.Widget)
			 */
			public void onClick(Widget sender) {
				if (!uicomponents.systemCheckBox.isVisible())
					uicomponents.systemCheckBox.setVisible(true);
				else if (uicomponents.systemCheckBox.isVisible())
					uicomponents.systemCheckBox.setVisible(false);
			}
		});
		panel.add(expand);

		return panel;
	}
	
	/**
	 * This method initiate the search request to the indexer
	 */
	public static void doSearch() {
		
		if (inputfield.input.getText().length() == 0)
			inputfield.input.setText(constants.input_field());
		else if(uicomponents.activated_system_list.size() == 0)
			inputfield.input.setText(constants.nothingtosearch());
		else { 
			RootPanel.get("searchresults").clear();
			searchResultHeadText.setVisible(true);
			searchText = inputfield.input.getText().toString();
			
			if(searchText.length() > 30){
				lucySearchForText.setHTML("<p>" + constants.you_searched()
						+ "<span class=\"highlight\"><b>" + searchText.substring(0, 30) + "..."
						+ "</b></span>.</p>");
			}else {
				lucySearchForText.setHTML("<p>" + constants.you_searched()
						+ "<span class=\"highlight\"><b>" + searchText
						+ "</b></span>.</p>");
			}
			
			// create loading
			uicomponents.createLoading();
			
			//start searching
			search();
			
			RootPanel.get("lucySearchForText").add(lucySearchForText);
		}
	}
	
	/**
	 * This method build a Panel with the normal search input field
	 * and with an Clicklistener to start the search method. 
	 * 
	 * @return FormPanel form
	 */
	public VerticalPanel buildMainSearchField() {
		
		VerticalPanel panel = new VerticalPanel();
		
		Button submit = new Button(constants.search(), new ClickListener() {
			/**
			 * @see com.google.gwt.user.client.ui.ClickListener#onClick(com.google.gwt.user.client.ui.Widget)
			 */
			public void onClick(Widget sender) {
				doSearch();
			}
		});
		
		HorizontalPanel submitExpand = new HorizontalPanel();
		submitExpand.setStyleName("submitExpand");
		
		VerticalPanel searchArea = buildSearchRoomTextField();
		searchArea.setStyleName("searchArea");
		
		submitExpand.add(searchArea);
		submitExpand.add(submit);
		
		submit.setStyleName("submitbutton");
//		panel.add(Input.buildSearchField());
		panel.add(submitExpand);

		return panel;
	}
	
	public static void initInputField() {
		RootPanel.get("inputfield").add(Input.buildSearchField());
	}

	/**
	 * This method split all search results into his own systems
	 * 
	 * @param JSONObject result
	 * @return VerticalPanel system result
	 */
	public VerticalPanel buildContent(JSONObject result) {
		
		VerticalPanel panel = new VerticalPanel();
		
		if (result.get("name").toString().replaceAll("\"", "").trim()
				.equals("Tarent Contact")) {
			panel.add(buildTarentContactContent(parseSearchResults(result,
					InitSearchParams.tcsearchParams)));
			
		} else if (result.get("name").toString().replaceAll("\"", "")
				.trim().equals("Intranet Wiki")) {
			panel.add(buildWikiContent(parseSearchResults(result,
					InitSearchParams.wikisearchParams)));
		} else if (result.get("name").toString().replaceAll("\"", "")
				.trim().equals("Infrastruktur Wiki")) {
			panel.add(buildWikiContent(parseSearchResults(result,
					InitSearchParams.wikisearchParams)));
		} else if (result.get("name").toString().replaceAll("\"", "")
				.trim().equals("Evolvis Platform")) {
			panel.add(buildEvolvisContent(parseSearchResults(result,
					InitSearchParams.evolvissearchParams)));
		}
		return panel;
	}
	
	/**
	 * This method put all search results into a panel
	 * 
	 * @param JSONObject result
	 * @return VerticalPanel all results
	 */
	public VerticalPanel buildAllContent(JSONObject result) {
		
		VerticalPanel panel = new VerticalPanel();
		
		if (result.get("name").toString().replaceAll("\"", "").trim()
				.equals("Tarent Contact")) {
			panel.add(buildTarentContactContent(parseSearchResults(result,
					InitSearchParams.tcsearchParams)));
			
		} else if (result.get("name").toString().replaceAll("\"", "")
				.trim().equals("Intranet Wiki")) {
			panel.add(buildWikiContent(parseSearchResults(result,
					InitSearchParams.wikisearchParams)));
		} else if (result.get("name").toString().replaceAll("\"", "")
				.trim().equals("Infrastruktur Wiki")) {
			panel.add(buildWikiContent(parseSearchResults(result,
					InitSearchParams.wikisearchParams)));
		} else if (result.get("name").toString().replaceAll("\"", "")
				.trim().equals("Evolvis Platform")) {
			panel.add(buildEvolvisContent(parseSearchResults(result,
					InitSearchParams.evolvissearchParams)));
		}
		return panel;
	}
	
	/**
	 * This method put all search results panels into a new Tab to have an overview
	 * about all search results.
	 * 
	 * @param Map list of search results panels
	 * @return VerticalPanel all search results
	 */
	public VerticalPanel buildAllContentView(Map<String, Widget> all) {
	
	VerticalPanel result = new VerticalPanel();
	VerticalPanel intranet = new VerticalPanel();
	VerticalPanel infrastruktur = new VerticalPanel();
	VerticalPanel evolvis = new VerticalPanel();
	VerticalPanel tarent_contact = new VerticalPanel();
	
	if (all.containsKey("Intranet Wiki") && intraid.equals("on")) {
		intranet.add(all.get("Intranet Wiki"));
	} 
	if(all.containsKey("Infrastruktur Wiki") && infraid.equals("on")) {
		infrastruktur.add(all.get("Infrastruktur Wiki"));
	}
	if(all.containsKey("Evolvis Platform") && evoid.equals("on")) {
		evolvis.add(all.get("Evolvis Platform"));
	} 
	if(all.containsKey("Tarent Contact") && tcid.equals("on")) {
		tarent_contact.add(all.get("Tarent Contact"));
	}
	
	result.add(intranet);
	result.add(infrastruktur);
	result.add(evolvis);
	result.add(tarent_contact);
	result.setStyleName("result-content");
	
	return result;
}
	
	/**
	 * This mehtod create the search result area for the ui
	 * 
	 * @param JSONArray results
	 */
	public void updateResultsArea(JSONArray results) {

		InitSearchParams.initTCSearchParams();// searchparams 4 tc
		InitSearchParams.initEvolvisSearchParams(); // searchparams 4 evolvis
		InitSearchParams.initWikiSearchParams(); // searchparams 4 all wikis


		VerticalPanel resultPanel = new VerticalPanel();

		resultTab = new TabPanel();
		infra = new VerticalPanel();
		intra = new VerticalPanel();
		evo = new VerticalPanel();
		tc = new VerticalPanel();
				
		infra_tmp = new VerticalPanel();
		intra_tmp = new VerticalPanel();
		evo_tmp = new VerticalPanel();
		tc_tmp = new VerticalPanel();
		
		Map<String, Widget> tab_map = new LinkedHashMap<String, Widget>();
		
		// set result count of each tab
		evoCount = 0;
		infraCount = 0;
		intraCount = 0;
		tcCount = 0;

		int resultCount = 0;
		int offlineSystems = 0;

		// check for activated systems an set the variables
		for (int j = 0; j < uicomponents.activated_system_list.size(); j++) {
			if (uicomponents.activated_system_list.get(j).equals(
					"Intranet Wiki")) {
				intraid = "on";
			} else if (uicomponents.activated_system_list.get(j).equals(
					"Infrastruktur Wiki")) {
				infraid = "on";
			} else if (uicomponents.activated_system_list.get(j).equals(
					"Tarent Contact")) {
				tcid = "on";
			} else if (uicomponents.activated_system_list.get(j).equals(
					"Evolvis Platform")) {
				evoid = "on";
			}
		}

		for (int i = 0; i < results.size(); i++) {
			JSONObject result = null;
			if ((result = results.get(i).isObject()) == null) {
				GWT.log("Result[" + i + "] not an object", null);
				continue;

			}

			if (result.get("systemNotAvailable") == null) {

				if (result.get("name").toString().replaceAll("\"", "").trim()
						.equals("Tarent Contact")) {
					tc.add(buildContent(result));
					tc_tmp.add(buildAllContent(result));
					tcid = "on";
					tcCount = tc.getWidgetCount();
				} else if (result.get("name").toString().replaceAll("\"", "")
						.trim().equals("Intranet Wiki")) {

					intra.add(buildContent(result));
					intra_tmp.add(buildAllContent(result));
					intraCount = intra.getWidgetCount();
					intraid = "on";

				} else if (result.get("name").toString().replaceAll("\"", "")
						.trim().equals("Infrastruktur Wiki")) {
					infra.add(buildContent(result));
					infra_tmp.add(buildAllContent(result));
					infraid = "on";
					infraCount = infra.getWidgetCount();
				} else if (result.get("name").toString().replaceAll("\"", "")
						.trim().equals("Evolvis Platform")) {
					evo.add(buildContent(result));
					evo_tmp.add(buildAllContent(result));
					evoid = "on";
					evoCount = evo.getWidgetCount();
				}
			} else {
				String systemName = result.get("systemNotAvailable").toString();

				if (systemName.equals("\"Tarent Contact\"")) {
					tc.add(new Label(systemName + " "
							+ constants.not_available()));
					offlineSystems++;
				}
				if (systemName.equals("\"Intranet Wiki\"")) {
					intra.add(new Label(systemName + " "
							+ constants.not_available()));
					offlineSystems++;
				}
				if (systemName.equals("\"Infrastruktur Wiki\"")) {
					infra.add(new Label(systemName + " "
							+ constants.not_available()));
					offlineSystems++;
				}
				if (systemName.equals("\"Evolvis Platform\"")) {
					evo.add(new Label(systemName + " "
							+ constants.not_available()));
					offlineSystems++;

				}
			}
		}
		
		//insert all search results in a new tab
		
		tab_map.put("Evolvis Platform", evo_tmp);
		tab_map.put("Infrastruktur Wiki", infra_tmp);
		tab_map.put("Intranet Wiki", intra_tmp);
		tab_map.put("Tarent Contact", tc_tmp);

		resultTab.add(buildAllContentView(tab_map),constants.result());
		
		// check if system is checked
		if (tcid.equals("on")) {
			resultTab.add(tc, "Tarent Contact - (" + tcCount + ")");
			
		}
		if(evoid.equals("on")){
			resultTab.add(evo,"Evolvis Platform - (" + evoCount + ")");
		}
		if (infraid.equals("on")) {
			resultTab.add(infra, "Infrastruktur Wiki - (" + infraCount + ")");
		}
		if(intraid.equals("on")){
			resultTab.add(intra,"Intranet Wiki - (" + intraCount + ")");
		}
		
		
		
		// check for empty widgets and the system is checked
		if (evo.getWidgetCount() == 0 && evoid.equals("on")) {
			evo.add(new HTML(constants.nothingFound()));
			evo_tmp.add(new HTML("<b>Evolvis Platform: </b>" + constants.nothingFound()));
		}
		if (infra.getWidgetCount() == 0 && infraid.equals("on")) {
			infra.add(new HTML(constants.nothingFound()));
			infra_tmp.add(new HTML("<b>Infrastruktur Wiki: </b>" + constants.nothingFound()));
		}
		if (intra.getWidgetCount() == 0 && intraid.equals("on")) {
			intra.add(new HTML(constants.nothingFound()));
			intra_tmp.add(new HTML("<b>Intranet Wiki: </b>" + constants.nothingFound()));
		}
		if (tc.getWidgetCount() == 0 && tcid.equals("on")) {
			tc.add(new HTML(constants.nothingFound()));
			tc_tmp.add(new HTML("<b>Tarent Contact: </b>" + constants.nothingFound()));
		}

		// set Style
		tc.setStyleName("result-content");
		intra.setStyleName("result-content");
		infra.setStyleName("result-content");
		evo.setStyleName("result-content");

		resultTab.selectTab(0);

		// count the results without the offline systems
		resultCount = results.size() - offlineSystems - 1;
		if (resultCount < 0) {
			resultCount = 0;
		}
		
		resultPanel.add(buildHeaderText(resultCount));
		
		resultPanel.add(resultTab);

		if (RootPanel.get("searchresults").isAttached()) {
			RootPanel.get("searchresults").clear();
			RootPanel.get("searchresults").add(resultPanel);
		} else {
			RootPanel.get("searchresults").add(resultPanel);
		}
		uicomponents.removeLoading();
	}
	
	/**
	 * This method parse all search results from the given system with defined params
	 * 
	 * @param Map result
	 * @param ArrayList params
	 * @return Map parsed_results
	 */
	public Map<String,String> parseSearchResults(JSONObject result, ArrayList<String> params) {
		Map<String, String> tmp = new LinkedHashMap<String, String>();
		
//		if(result.get("name").equals("Intranet Wiki")) {
//			System.out.println(result.get("data").toString().matches("[*|*]"));
//		}
		
		if(result == null){
			tmp.put("info", "keine Suchergebnisse vorhanden");
		}else {
			for(int i = 0;i < params.size();i++){
				if(result.get(params.get(i)) != null){	
					tmp.put(params.get(i), result.get(params.get(i)).toString().replaceAll("\"", "").trim());
//					parseWikiSyntax(result);
				} else {
					tmp.put(params.get(i), "n/a");
				}
			}
		}
		return tmp;
	}
	
	/**
	 * This Method parse all wikipedia syntax into HTML
	 */
//	public Map<String, String> parseWikiSyntax(JSONObject results) {
//		
//		tmp = new LinkedHashMap<String, String>();
//		String parseWikiSyntax = null;
//		
//		for(int i = 0;i < results.size();i++){
//			 if(results.get("data").toString().equals(parser.bold)){
//				 tmp.put("data", results.get("data").toString().replaceAll(parser.bold, "<b>*</b>"));
//			 }
//		}
//		return tmp;
//	}
	
	
	
	/**
	 * This mehtod create the search result area for Evolvis
	 * 
	 * @param Map parsed_search_results
	 * @return VerticalPanel evolvis result field
	 */
	public VerticalPanel buildEvolvisContent(Map<String,String> parsed_search_results) {
		
		VerticalPanel resultField = new VerticalPanel();
		resultField.setStyleName("result-items");

		HTML header_content = new HTML("<i><b>" + parsed_search_results.get("name") + "</b></i>" + " - "
				+ "<a target=\"_blank\" href=\"" + parsed_search_results.get("url") + "\">" + parsed_search_results.get("title") + "</a>");

		resultField.add(header_content);
		
		return resultField;
	}
	
	/**
	 * This mehtod create the search result area for all wikis
	 * 
	 * @param Map parsed_search_results
	 * @return VerticalPanel wiki results
	 */
	public VerticalPanel buildWikiContent(
			Map<String, String> parsed_search_results) {

		VerticalPanel resultField = new VerticalPanel();
		resultField.setStyleName("result-items");

		Image systemImage = null;
		String contentType = parsed_search_results.get("contentType");
		String t = parsed_search_results.get("data");
//		String data = t.replaceAll("\\\n", "...");

//		if(t.equals(searchText)) {
		data = t.replaceAll(searchText, "<font color=#000000>" + searchText + "</font>");
//		datacount++;
//		System.out.println(data.indexOf(searchText));
		
//		}else {
//		data = t;	
//		}
	
//		System.out.println(data.toString());
		
		if (contentType.equals("bitblogpost")
				|| contentType.equals("wiki_blog")
				|| contentType.equals("bitbook")
				|| contentType.equals("bitpage")) {
			systemImage = textIcon;
			systemImage.setStyleName("systemIcon"); // set style of icons
			systemImage.setTitle(" " + constants.text() + " ");
		} else if (contentType.equals("fisheyegallery")
				|| contentType.equals("fisheyeimage")
				|| contentType.equals("pigeonholes")) {
			systemImage = picIcon;
			systemImage.setStyleName("systemIcon"); // set style of icons
			systemImage.setTitle(" " + constants.pic() + " ");
		}

		HTML header_content;
		
		// main content eg url, title, icon etc.
		header_content = new HTML("<i><b>"
					+ parsed_search_results.get("name") + "</b></i>" + " - "
					+ "<a target=\"_blank\" href=\"" + parsed_search_results.get("url") + "\">"
					+ parsed_search_results.get("title") + "</a>" + " - "
					+ systemImage);

		HTML content = null;
//		data = data.replaceAll("\\n", "...");
		// a little bit text
		if (data.length() > 100)
			content = new HTML(data.substring(0, 100) + "<b>...</b>");
		if (data.length() > 280)
			content = new HTML(data.substring(0, 200) + "<b>...</b>");
		if (data.length() < 100)
			content = new HTML(data + "<b>...</b>");
//		if(data.equals("\n"))
//			content = new HTML(data.replaceAll("\n", "..."));

		content.setStyleName("search-data");

		resultField.add(header_content);
		resultField.add(content);

		return resultField;
	}
	
	/**
	 * This method create the search result area for tarent contact
	 * search results
	 * 
	 * @param Map parsed_search_results
	 * @return VerticalPanel tarent contact results
	 */
	public VerticalPanel buildTarentContactContent(Map<String,String> parsed_search_results) {
		
		VerticalPanel resultField = new VerticalPanel();
		resultField.setStyleName("result-items");
		
		HTML header_content = new HTML("<i><b>" + parsed_search_results.get("herrnFrau") + 
				 " " + parsed_search_results.get("vorname") + " " + parsed_search_results.get("nachname") + "</b></i>" + " - "
				  + parsed_search_results.get("institution"));;
		
		DisclosurePanel panel = new DisclosurePanel(header_content);
		
		FlexTable table = new FlexTable();
		table.setWidget(0, 0, new HTML("<b>" + constants.tc_adress() + "</b>"));
		table.setWidget(1, 0, new HTML("<i>" + constants.tc_adress_street() + ": </i>"));
		table.setText(1, 1, parsed_search_results.get("strasse") + " " + parsed_search_results.get("hausNr"));
		table.setWidget(2, 0, new HTML("<i>" + constants.tc_adress_postcode() + "/" + constants.tc_adress_city() + ": </i>"));
		table.setText(2, 1, parsed_search_results.get("plz") + " " + parsed_search_results.get("ort"));
		
		table.setText(3, 0,"");
		table.setText(4, 0,"");
		
		table.setWidget(5, 0, new HTML("<b>" + constants.tc_communication_commercial() +"</b>"));
		table.setWidget(6, 0, new HTML("<i>" + constants.tc_communication_commercial_phone() + ": </i>"));
		table.setText(6, 1, parsed_search_results.get("telefon"));
		table.setWidget(7, 0, new HTML("<i>" + constants.tc_communication_commercial_email() + ": </i>"));
		table.setText(7, 1, parsed_search_results.get("eMail"));
		table.setWidget(8, 0, new HTML("<i>" + constants.tc_communication_commercial_mobile() + ": </i>"));
		table.setText(8, 1, parsed_search_results.get("telefonMobil"));
		table.setWidget(9, 0, new HTML("<i>" + constants.tc_communication_commercial_fax() + ": </i>"));
		table.setText(9, 1, parsed_search_results.get("fax"));
		table.setWidget(10, 0, new HTML("<i>" + constants.tc_communication_commercial_homepage() + ": </i>"));
		table.setText(10, 1, parsed_search_results.get("homepage"));
		
		table.setText(11, 0, "");
		table.setText(12, 0, "");
		
		table.setWidget(13, 0, new HTML("<b>" + constants.tc_communication_private() + " </b>"));
		table.setWidget(14, 0, new HTML("<i>" + constants.tc_communication_private_phone() + ": </i>"));
		table.setText(14, 1, parsed_search_results.get("telefonPrivat"));
		table.setWidget(15, 0, new HTML("<i>" + constants.tc_communication_private_email() + ": </i>"));
		table.setText(15, 1, parsed_search_results.get("eMailPrivat"));
		table.setWidget(16, 0, new HTML("<i>" + constants.tc_communication_private_mobile()+ ": </i>"));
		table.setText(16, 1, parsed_search_results.get("telefonMobilPrivat"));
		table.setWidget(17, 0, new HTML("<i>" + constants.tc_communication_private_fax()+ ": </i>"));
		table.setText(18, 1, parsed_search_results.get("faxPrivat"));

		panel.add(table);
		
		resultField.add(panel);
		
		return resultField;
	}
}


/* dynamic checkboxes
 *		(Content)updateResults
		String check = results.get(0).toString();
		if(check.equals("\"search\"")){
			bereits vorhandenes
		}
		if(check.equals("\"refresh\"") || check == "refresh"){
			ArrayList<String> systems = new ArrayList<String>();
			for(int i = 0;i < results.size();i++){
				systems.add(results.get(i).toString());
			}
			uicomponents.refreshCheckBoxes(systems);
			panel.add(uicomponents.generateSystems());
			RootPanel.get("searchroom").add(buildSearchRoomTextField());
			
		}
 */


