/*
 * lucy-ui,
 * lucy user interface
 * Copyright (C) 2000-2009 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'lucy-ui'
 * Signature of Elmar Geese, 21 April 2009
 * Elmar Geese, CEO tarent GmbH.
 */

/**
 * LucyIndexerPortType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package org.evolvis.lucy.ui.server;

public interface LucyIndexerPortType extends java.rmi.Remote {
    public void cleanup(java.lang.String username, java.lang.String password) throws java.rmi.RemoteException;
    public java.lang.Object[] query(java.lang.String username, java.lang.String password, java.lang.String s, java.lang.String q) throws java.rmi.RemoteException;
    public void login_SOAP(java.lang.String username, java.lang.String password) throws java.rmi.RemoteException;
    public void showLogin(java.lang.String username, java.lang.String password) throws java.rmi.RemoteException;
    public void login(java.lang.String username, java.lang.String password) throws java.rmi.RemoteException;
    public void rebuild(java.lang.String username, java.lang.String password) throws java.rmi.RemoteException;
    public void logout_SOAP(java.lang.String username, java.lang.String password) throws java.rmi.RemoteException;
    public void init(java.lang.String username, java.lang.String password) throws java.rmi.RemoteException;
    public java.lang.Object autostart(java.lang.String username, java.lang.String password) throws java.rmi.RemoteException;
    public void _default(java.lang.String username, java.lang.String password) throws java.rmi.RemoteException;
    public void logout(java.lang.String username, java.lang.String password) throws java.rmi.RemoteException;
}
