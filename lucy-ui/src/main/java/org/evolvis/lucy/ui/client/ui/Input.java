/*
 * lucy-ui,
 * lucy user interface
 * Copyright (C) 2000-2009 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'lucy-ui'
 * Signature of Elmar Geese, 21 April 2009
 * Elmar Geese, CEO tarent GmbH.
 */

package org.evolvis.lucy.ui.client.ui;

import java.util.ArrayList;

import org.evolvis.lucy.ui.client.i18n.InternationalizationConstants;
import org.evolvis.lucy.ui.client.req.SearchResultHandler;
import com.google.gwt.core.client.GWT;
import com.google.gwt.json.client.JSONArray;
import com.google.gwt.json.client.JSONObject;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.ClickListener;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.KeyboardListener;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.MultiWordSuggestOracle;
import com.google.gwt.user.client.ui.SuggestBox;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.Widget;

/**
 * This class contains all method for the search input field
 * 
 * @author Viktor Hamm tarent GmbH Bonn
 *
 */
public class Input {

	static InternationalizationConstants constants = (InternationalizationConstants) GWT
	.create(InternationalizationConstants.class);
	
	public static ArrayList<String> oracle = new ArrayList<String>();
	public static MultiWordSuggestOracle dataInput;
	public static SuggestBox input;
	public static HorizontalPanel mainSearchField;
	public static Label includeField;
	public static String searchtext;
	//public static String id = "on";
	
	static TextBox inputTextBox;
	
	/**
	 * This method parsed all oracle suggest results into an array
	 * 
	 * @param String oracle_data
	 * @return Array oracle sugesst results
	 */
	public static String[] parseOracleSuggestData(String oracle_data) {
		String tmp = oracle_data.substring(0,oracle_data.length()-1).trim();
		tmp = tmp.substring(1).trim();
		String[] parsed_results = tmp.split(",");
		return parsed_results;
	}
	
	/**
	 * This is the MultiWordSuggestOracle method. It includes all Data from the
	 * user search input and the title from the search results in the Popup from the SuggestBox.
	 * The User types an word, then will be the popup shown with an suggestion
	 * 
	 * @return MultiWordSuggestOracle datainput
	 */
	public static MultiWordSuggestOracle createDataInput(JSONArray results) {

		MultiWordSuggestOracle dataInput = new MultiWordSuggestOracle();
		
		String title = null;
		String firstname = null;
		String lastname = null;

		if (results != null) {

			// get the previous suggest list from the hidden field
			String each_value = Content.oracle_content.getValue();
			if (!each_value.equals("")) {

				// parse the previus suggest list and add them into
				// the MultiWordSuggestOracle list.
				String[] oracle_results = parseOracleSuggestData(each_value);
				if(oracle_results.length < 100) {
					for (int i = 0; i < oracle_results.length; i++) {
						dataInput.add(oracle_results[i]);
					}
				}else {
					for (int i = 0; i < 100; i++) {
						dataInput.add(oracle_results[i]);
					}
				}
			}

			// add the user search input to the MultiWordSuggestOracle list
			String searchtext = input.getText().toString();
			dataInput.add(searchtext);

			for (int i = 0; i < results.size(); i++) {
				JSONObject result = null;

				if ((result = results.get(i).isObject()) == null) {
					GWT.log("Result[" + i + "] not an object", null);
					continue;
				}
				// add the result title in the MultiWordSuggestOracle list
				if (result.get("title") != null) {
					title = result.get("title").toString().replaceAll("\"", "")
							.trim();
					
					oracle.add(searchtext);
					oracle.add(title);
					
					dataInput.add(title);

				} else {
					// add the first and last name in the MultiWordSuggestOracle
					// list
					// when the search results don't have an title
					firstname = result.get("vorname").toString().replaceAll(
							"\"", "").trim();
					lastname = result.get("nachname").toString().replaceAll(
							"\"", "").trim();

					oracle.add(firstname + " " + lastname);
					dataInput.add(firstname + " " + lastname);
				}
			}
			
			Content.oracle_content.setValue(oracle.toString());
		}
		return dataInput;
	}
		
	/**
	 * This method build the normal search field with an HorizontalPanel for the
	 * end user. Also its includes an autocomplete method by the SuggestBox
	 * Widget
	 * 
	 * @return HorizontalPanel mainSearchField
	 */
	public static HorizontalPanel buildSearchField() {

		inputTextBox = new TextBox();
		inputTextBox.setFocus(true); // does not work with Firefox
		inputTextBox.addClickListener(new ClickListener() {
			
			public void onClick(Widget sender) {
				if (input.getText().equals(constants.input_field()))
					input.setText("");
				
				if (input.getText().equals(constants.nothingtosearch()))
					input.setText("");
			}

		});
		// TODO
		
//		input.addEventHandler(new SuggestionHandler() {
//
//			public void onSuggestionSelected(SuggestionEvent event) {
//				Window.alert(event.getSelectedSuggestion().getReplacementString().trim());
//				searchtext = event.getSelectedSuggestion().getReplacementString().trim();
//			}});
		
		inputTextBox.setName("inputfield");
		
		if(inputTextBox.isAttached()){
			inputTextBox.setFocus(true);
		}
		
		mainSearchField = new HorizontalPanel();
		input = new SuggestBox(createDataInput(SearchResultHandler.results),
				inputTextBox);
		input.setLimit(10);
// TODO
// wait for new GWT Release 
// 
//		input.addKeyboardListener(new KeyboardListener(){
//
//			@Deprecated
//			public void onKeyDown(Widget sender, char keyCode, int modifiers) {//				
//			}
//
//			public void onKeyPress(Widget sender, char keyCode, int modifiers) {
//				
//				if(keyCode == 13 )
//				{
//					Window.alert("key: "+sender.toString());
//					if(input.isVisible())
//					{
//						Window.alert("!empty: "+sender.toString());
//						Window.alert(searchtext);
//						if(!input.getText().equals(Content.searchText))
//						{
//							Content.searchText = input.getText();
//							Window.alert("input.getText(): "+input.getText()
//									+"\nContent.searchText: "+Content.searchText);
//						}
//						Content.oracle_content.setValue(searchtext);
//						input.setText(searchtext); // you searched for <searchtext>
//						Content.searchText = input.getText();
////						Window.alert(event.getSelectedSuggestion().getReplacementString().trim().toString());
//					} else
//					{
//						Content.doSearch();
//					}
//				}
//				int s = 0;
//				if(keyCode == 13) {
//					
//					if(input.isVisible()) {
//						Window.alert("empty" + sender.toString());
//						if(!input.getText().equals(Content.searchText))
//							{
//								Content.searchText = input.getText();
//								Window.alert("input.getText(): "+input.getText()
//										+"\nContent.searchText: "+Content.searchText);
//								
//							}
//					}else {
//						Content.doSearch();
//					}
//					
//				}
//				
//			}
//
//			@Deprecated
//			public void onKeyUp(Widget sender, char keyCode, int modifiers) {				
//			}});
		
		if(Content.user_input != null){
			input.setText(Content.user_input.getValue().toString());
		}
	
		includeField = new Label(constants.search_for());

		mainSearchField.add(includeField);
		mainSearchField.add(input);

		mainSearchField.setStyleName("searchfield");
		mainSearchField.setSpacing(10);

		return mainSearchField;
	}
}
