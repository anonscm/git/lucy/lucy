/*
 * lucy-ui,
 * lucy user interface
 * Copyright (C) 2000-2009 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'lucy-ui'
 * Signature of Elmar Geese, 21 April 2009
 * Elmar Geese, CEO tarent GmbH.
 */

package org.evolvis.lucy.ui.client.ui;

import java.util.ArrayList;
import org.evolvis.lucy.ui.client.i18n.InternationalizationConstants;
import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.ClickListener;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.user.client.ui.TabBar;
import com.google.gwt.user.client.ui.TabPanel;
import com.google.gwt.user.client.ui.Widget;

/**
 * This class contains method to generate the lucy user interface
 * 
 * @author Viktor Hamm tarent GmbH Bonn
 *
 */
public class UIComponents {
	
	Input inputfield = new Input();
	
	static InternationalizationConstants constants = (InternationalizationConstants) GWT
	.create(InternationalizationConstants.class);
	
	/**
	 * This is a test String with search systems because at the moment
	 * we can't automatically includes all regist systems from the 
	 * Lucy Indexer
	 */
	public static final String[] SYSTEM_TYPES = new String[] { "Evolvis Platform",
			"Intranet Wiki", "Infrastruktur Wiki", "Tarent Contact" };
	
	public FlowPanel systemCheckBox;
	public CheckBox systemTypes = new CheckBox();
	public HTML lucydesc;
		
	public ArrayList<String> activated_system_list = new ArrayList<String>();
	
	public HTML aboutLucy;
	
	public PopupPanel loadingPopup = new PopupPanel(false);
	
	/**
	 * This class is the Clicklistener for all system check boxes.
	 * The class will be need to develop an dynamic search result
	 * filtering by activation/deactivation a system check box.
	 * 
	 * @author Viktor Hamm tarent GmbH Bonn
	 *
	 */
	public class SystemCheckBoxFilter implements ClickListener {

		public CheckBox systems;
		public String name;
		
		
		public SystemCheckBoxFilter(CheckBox systems) {
			this.systems = systems;
		}

		/**
		 * @see com.google.gwt.user.client.ui.ClickListener#onClick(com.google.gwt.user.client.ui.Widget)
		 */
		public void onClick(Widget sender) {
			
			if(Content.resultTab != null)
			{
				TabPanel resultTab = Content.resultTab;
				TabBar resultTabBar = Content.resultTab.getTabBar();
				if (systems.isChecked()) // set off to on 
				{
					if (!activated_system_list.contains(systems.getName()))
						activated_system_list.add(systems.getName());
					if(resultTab.getWidgetCount() != 0)
					{
						for (int i = 0; i < resultTab.getWidgetCount(); i++) {
							if(systems.getName().equals("Evolvis Platform"))
							{
								resultTab.add(Content.evo, systems.getName() + " - (" + Content.evoCount + ")");
							}else if(systems.getName().equals("Intranet Wiki"))
							{
								resultTab.add(Content.intra, systems.getName() + " - (" + Content.intraCount + ")");
							}else if(systems.getName().equals("Infrastruktur Wiki"))
							{
								resultTab.add(Content.infra, systems.getName() + " - (" + Content.infraCount + ")");
							}else if(systems.getName().equals("Tarent Contact"))
							{
								resultTab.add(Content.tc, systems.getName() + " - (" + Content.tcCount + ")");
							}
							resultTab.selectTab(i);
						}
					}
				} else if (!systems.isChecked()) // set on to off 
				{
					activated_system_list.remove(systems.getText());
					if(resultTab.getWidgetCount() == 1)
					{
						systems.setChecked(true);
					}else if(resultTab.getWidgetCount() != 0)
					{
						for (int i = 0; i < resultTab.getWidgetCount(); i++) {
							String tabText = resultTabBar.getTabHTML(i);
							String[] tabTextArray = tabText.split(" - ");
							String sysName = systems.getName();

							if(tabTextArray[0].equals(sysName))
							{
								resultTab.remove(i);
								resultTab.selectTab(0);
							}
						}
					}

				}
			} else
			{
				if (systems.isChecked()) { // set off to on
					if (!activated_system_list.contains(systems.getName()))
						activated_system_list.add(systems.getName());
				} else if (!systems.isChecked()) { // set on to off
					activated_system_list.remove(systems.getText());
				}
			}
		}
	}
	
	/**
	 * This method builds all regist systems from the index, automatically.
	 * 
	 * TODO: systems generated by reading the regestry from the indexer
	 * 
	 * @return FlowPanel systemsOrder
	 */
	public FlowPanel generateSystems() {

		activated_system_list.clear();
		
		systemCheckBox = new FlowPanel();
		systemCheckBox.setStyleName("checkboxfield");

		for (int i = 0; i < SYSTEM_TYPES.length; i++) {
			systemTypes = new CheckBox(SYSTEM_TYPES[i]);
			systemTypes.setName(SYSTEM_TYPES[i]);
			systemTypes.setChecked(true);
			systemTypes.addClickListener(new SystemCheckBoxFilter(systemTypes));
			systemTypes.addStyleName("checkbox");

			activated_system_list.add(i, SYSTEM_TYPES[i]);

			systemCheckBox.add(systemTypes);
			systemCheckBox.setVisible(false);
		}
		return systemCheckBox;
	}
	
	/**
	 * This method builds the main Lucy description field
	 * 
	 * @return HTML lucydesc
	 */
	public HTML buildDescField() {

		lucydesc = new HTML("<a class=\"lucyDesc\">" + constants.about_lucy() + "</a>");
		lucydesc.setStyleName("lucydesctext");
	
		aboutLucy.setVisible(false);

		lucydesc.addClickListener(new ClickListener() {
			
			public void onClick(Widget sender) {
				if(aboutLucy.isVisible())
				{
					aboutLucy.setVisible(false);
				}else if(!aboutLucy.isVisible())
				{
					aboutLucy.setVisible(true);
				}
			}});
		
		return lucydesc;
	}
	
	public HTML aboutLucyText()
	{
		aboutLucy = new HTML("<h1 lang=\"en\">" + constants.about_lucy()
				+ "</h1> <p> " + constants.about_lucy_text() + "</p>");
		aboutLucy.setStyleName("aboutLucy");
		
		return aboutLucy;
	}
	
	public void createLoading()
	{
//		VerticalPanel loadingPanel = new VerticalPanel();
//		loadingPanel.setStyleName("loadingPanel");
//		loadingPanel.setHeight(Window.getClientHeight()+"");
//		loadingPanel.setWidth(Window.getClientWidth()+"");
//		
//		RootPanel.get().add(loadingPanel);
		
		loadingPopup.setStyleName("loadingPopup");
		
		HorizontalPanel loadingPopupContent = new HorizontalPanel();
		
		Image loadingImage = new Image("img/loading.gif");
		loadingImage.setStyleName("loadingImage");
		Label loadingText = new Label("loading results");
		loadingText.setStyleName("loadingText");
		
		loadingPopupContent.add(loadingImage);
		loadingPopupContent.add(loadingText);
		
		loadingPopup.setWidget(loadingPopupContent);
		loadingPopup.center();
	}
	
	public void removeLoading()
	{
		loadingPopup.hide();
	}
}
