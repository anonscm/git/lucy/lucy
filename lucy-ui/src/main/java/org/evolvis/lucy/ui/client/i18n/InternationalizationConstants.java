/*
 * lucy-ui,
 * lucy user interface
 * Copyright (C) 2000-2009 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'lucy-ui'
 * Signature of Elmar Geese, 21 April 2009
 * Elmar Geese, CEO tarent GmbH.
 */

package org.evolvis.lucy.ui.client.i18n;

import com.google.gwt.i18n.client.Constants;

/**
 * Interface for internationalization in GWT
 * lucy internationalization
 * 
 * @author fesser
 */
public interface InternationalizationConstants extends Constants {
	String about_lucy();
	String about_lucy_text();
	String search();
	String input_field();
	String you_searched();
	String search_for();
	String unhappy();
	String more_results();
	String expand_area();
	String best_hits();
	String lucy_engine();
	String not_available();
	String ergFound();
	String nothingFound();
	String text();
	String pic();
	String result();
	String nothingtosearch();
	String intra_headline();
	String infra_headline();
	String evo_headline();
	String tc_headline();
	String tc_adress();
	String tc_adress_street();
	String tc_adress_postcode();
	String tc_adress_city();
	String tc_communication_commercial();
	String tc_communication_commercial_phone();
	String tc_communication_commercial_email();
	String tc_communication_commercial_mobile();
	String tc_communication_commercial_fax();
	String tc_communication_commercial_homepage();
	String tc_communication_private();
	String tc_communication_private_phone();
	String tc_communication_private_email();
	String tc_communication_private_mobile();
	String tc_communication_private_fax();
}
