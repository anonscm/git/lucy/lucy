/*
 * lucy-ui,
 * lucy user interface
 * Copyright (C) 2000-2009 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'lucy-ui'
 * Signature of Elmar Geese, 21 April 2009
 * Elmar Geese, CEO tarent GmbH.
 */

package org.evolvis.lucy.ui.client.config;

import org.evolvis.lucy.ui.client.LucySearchModul;

import com.google.gwt.http.client.RequestBuilder;
import com.google.gwt.http.client.RequestException;
import com.google.gwt.user.client.Window;

/**
 * @author Frederic Eßer
 * 
 * This class is to read the data for the communication 
 * to the lucy-indexer 
 * 
 */
public class LucyConfig {
	
	static String fileContent = "";
	static Boolean callbackComplete = false;
	static RequestBuilder requestBuilder = new RequestBuilder(RequestBuilder.GET,"lucy-uiconfig.xml");
	
	/**
	 * @author Frederic Eßer
	 * 
	 * This method read the content of the lucy-uiconfig.xml file
	 * 
	 * @param fileName, lucySearchModul
	 * 
	 * @return fileContent 
	 */
	public static void getXMLContent(LucySearchModul lucySearchModul)
	{		
		try
		{
			requestBuilder.sendRequest(null, new LucyRequestCallback(lucySearchModul));
		} catch (RequestException ex)
		{
			requestFailed(ex);
		}
	}
	
	/**
	 * @author Frederic Eßer
	 * 
	 * This method handle the exception if send Request failed
	 * 
	 * @param exception
	 */
	public static void requestFailed(Throwable exception)
	{
		Window.alert("Request failed. Error was: " + exception.getMessage());
	}
}