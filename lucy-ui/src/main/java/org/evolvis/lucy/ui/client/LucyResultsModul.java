/*
 * lucy-ui,
 * lucy user interface
 * Copyright (C) 2000-2009 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'lucy-ui'
 * Signature of Elmar Geese, 21 April 2009
 * Elmar Geese, CEO tarent GmbH.
 */

package org.evolvis.lucy.ui.client;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.user.client.ui.ClickListener;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.VerticalPanel;

/**
 * This Class is the Results Modul for the lucy-results.html
 * 
 * It includes the main search field, the extendet search field with 
 * all automatically generated system check boxes and a search result table.
 * 
 * @author Viktor Hamm tarent GmbH Bonn
 * 
 */
@Deprecated
public class LucyResultsModul extends LucyModule implements EntryPoint {

	/**
	 * This is the entry point method. You need it, when you will use a Modul in
	 * a HTML File
	 * 
	 * @see com.google.gwt.core.client.EntryPoint#onModuleLoad()
	 */
	public void onModuleLoad() {
		
		HTML lucyResultsHeadline = new HTML("<h1>lucy, your search results</h1>");
		HTML lucySearchForText = new HTML("<p>You searched for <span class=\"highlight\">" + searchText +  "</span>.</p>");

		RootPanel.get("lucyresultsheadline").add(lucyResultsHeadline);
		RootPanel.get("lucySearchForText").add(lucySearchForText);
		
		/**
		 * the main form with an search field and extendet field with 
		 * all generated check boxes
		 */
		RootPanel.get("mainform").add(buildMainFormField());
		
		/**
		 * HTML Widgets for the description text and the search room text
		 * with an link to set the extendet search field on visible because
		 * this field is unvisible while the first start of the result page
		 */
		RootPanel.get("searchroom").add(buildSearchRoomTextField());
		RootPanel.get("lucydesctext").add(buildDescField());
		
		/**
		 * two methods to build the results field
		 */
		RootPanel.get("searchResultHeadText").add(buildHeaderText());
		RootPanel.get("searchresultfield").add(buildSearchResultField());

	}

	/**
	 * This Method build the search room text with an link to be set the extendet
	 * search field on visible
	 * 
	 * @return VerticalPanel panel
	 */
	public VerticalPanel buildSearchRoomTextField() {

		VerticalPanel panel = new VerticalPanel();
		
		HTML headline = new HTML("<p><h1>unhappy?</h1>for more search results, you can");
		HTML expand = new HTML("<a>expand the search area</a></p>");
		
		if (systemCheckBox.isVisible())
			systemCheckBox.setVisible(false);
		
		expand.addClickListener(new ClickListener(){

			/**
			 * @see com.google.gwt.user.client.ui.ClickListener#onClick(com.google.gwt.user.client.ui.Widget)
			 */
			public void onClick(Widget sender) {
				// TODO Auto-generated method stub
				systemCheckBox.setVisible(true);
			}
		});
		panel.add(headline);
		panel.add(expand);

		return panel;
	}
	
	/**
	 * the static header text for the search result field
	 * 
	 * @return HTML resultHeadText
	 */
	public static HTML buildHeaderText() {
		HTML searchResultHeadText = new HTML("<h2>the best 5 hits</h2>");
		return searchResultHeadText;
	}
	
	/**
	 * This method build a table with the search results.
	 * At the moment only with test data
	 * 
	 * @return VerticalPanel resultField
	 */
	public VerticalPanel buildSearchResultField() {
		resultField = new VerticalPanel();
		resultField.add(buildData(links,icons));
		return resultField;
	}
	
	/**
	 * Test String with links
	 */
	public static final String[] links = {
		"<a href=\"http://wiki.tarent.de/wikis/infrastruktur/wiki/index.php?content_id=54&amp;highlight=N800\">N800 Display-Ausschaltzeit einstellen</a>",
		"<a href=\"http://wiki.tarent.de/wikis/infrastruktur/wiki/index.php?content_id=55&amp;highlight=N800\">Navigation mit dem Nokia IT</a>",
		"<a href=\"http://wiki.tarent.de/wikis/infrastruktur/wiki/index.php?content_id=54&amp;highlight=N800\">N800 Display-Ausschaltzeit einstellen</a>",
		"<a href=\"http://.tarent.de/wikis/infrastruktur/wiki/index.php?content_id=21&amp;highlight=N800\">osso-xterm</a>"
	};
	
	/**
	 * Test string with icons
	 */
	public static final String[] icons = {
		"<img src=\"img/icons/tango/mimetypes/text-x-generic.png\" alt=\"Textdokument\"/>",
		"<img src=\"img/icons/tango/mimetypes/image-x-generic.png\" alt=\"Bilddokument\"/>"
	};
	
	/**
	 * This method build the search result table with an FlexTable Widget by
	 * test data.
	 * 
	 * @param links
	 * @param icons
	 * @return Flextable resultData
	 */
	public FlexTable buildData(String links[], String icons[]) {
		
		FlexTable resultData = new FlexTable();
		resultData.setPixelSize(490, 0);
		
		/**
		 * builds the table with test data
		 */
		for(int i = 0; i < links.length;i++)
		resultData.setHTML(i, 0, links[i] + "<br>" + icons[0] + " - " + "Infrastruktur Wiki");
		
		return resultData;
	}
	
}