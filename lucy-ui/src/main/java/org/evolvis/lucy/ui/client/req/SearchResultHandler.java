/*
 * lucy-ui,
 * lucy user interface
 * Copyright (C) 2000-2009 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'lucy-ui'
 * Signature of Elmar Geese, 21 April 2009
 * Elmar Geese, CEO tarent GmbH.
 */

package org.evolvis.lucy.ui.client.req;

import org.evolvis.lucy.ui.client.ui.Content;

import org.evolvis.lucy.ui.client.ui.Input;
import com.google.gwt.core.client.GWT;
import com.google.gwt.http.client.Request;
import com.google.gwt.http.client.RequestCallback;
import com.google.gwt.http.client.Response;
import com.google.gwt.json.client.JSONArray;
import com.google.gwt.json.client.JSONException;
import com.google.gwt.json.client.JSONParser;
import com.google.gwt.json.client.JSONValue;
import com.google.gwt.user.client.ui.RootPanel;

/**
 * This is an handler for all search results
 * 
 * @author Viktor Hamm tarent GmbH Bonn
 *
 */
public class SearchResultHandler implements RequestCallback {
	
	public Content uicontent = new Content();
	public static JSONArray results;
	public Input input = new Input();
	
	/**
	 * @see com.google.gwt.http.client.RequestCallback#onError(com.google.gwt.http.client.Request, java.lang.Throwable)
	 */
	public void onError(Request request, Throwable exception) {
		System.out.println("lucy-ui request exception: " + exception.toString());
	}

	/**
	 * @see com.google.gwt.http.client.RequestCallback#onResponseReceived(com.google.gwt.http.client.Request, com.google.gwt.http.client.Response)
	 */
	public void onResponseReceived(Request request, Response response) {
		results = extractLucyResults(response.getText());
		RootPanel.get("inputfield").clear();
		Content.initInputField();
		uicontent.updateResultsArea(results);
	}

	/**
	 * This method extract the search results from server response
	 * and put them into an JSONArray
	 * 
	 * @param responseText
	 * @return JSONArray results
	 */
	private JSONArray extractLucyResults(String responseText) {
		JSONValue resVal;
		JSONArray resArray;
		if (responseText == null || responseText.equals("")) {
			GWT.log("no response content", null);
			return null;
		}

		try {
			resVal = JSONParser.parse(responseText);
		} catch (JSONException e) {
			GWT.log("JSONParseException:" + responseText, e);
			return null;
		}
		if ((resArray = resVal.isArray()) == null) {
			GWT.log("resObject is unexpected type.", null);
		}
		return resArray;
	}
}
