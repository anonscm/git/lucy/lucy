/*
 * lucy-ui,
 * lucy user interface
 * Copyright (C) 2000-2009 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'lucy-ui'
 * Signature of Elmar Geese, 21 April 2009
 * Elmar Geese, CEO tarent GmbH.
 */

package org.evolvis.lucy.ui.client.ui;

import java.util.ArrayList;

/**
 * In this class you can set your search params
 * 
 * @author Viktor Hamm tarent GmbH Bonn
 * 
 */
public class InitSearchParams {

	public static ArrayList<String> tcsearchParams;
	public static ArrayList<String> wikisearchParams;
	public static ArrayList<String> evolvissearchParams;

	/**
	 * Evolvis search params
	 */
	public static void initEvolvisSearchParams() {

		evolvissearchParams = new ArrayList<String>();
		evolvissearchParams.add("content_id");
		evolvissearchParams.add("contentType");
		evolvissearchParams.add("data");
		evolvissearchParams.add("title");
		evolvissearchParams.add("url");
		evolvissearchParams.add("name");

	}

	/**
	 * Wiki search params
	 */
	public static void initWikiSearchParams() {

		wikisearchParams = new ArrayList<String>();
		wikisearchParams.add("content_id");
		wikisearchParams.add("contentType");
		wikisearchParams.add("data");
		wikisearchParams.add("title");
		wikisearchParams.add("url");
		wikisearchParams.add("name");

	}

	/**
	 * Tarent Contact search params
	 */
	public static void initTCSearchParams() {

		tcsearchParams = new ArrayList<String>();
		tcsearchParams.add("anrede");
		tcsearchParams.add("letteraddress");
		tcsearchParams.add("institutionErweitert");
		tcsearchParams.add("strasse");
		tcsearchParams.add("position");
		tcsearchParams.add("hausNr");
		tcsearchParams.add("fk_user_followup");
		tcsearchParams.add("followup");
		tcsearchParams.add("homepage");
		tcsearchParams.add("fax");
		tcsearchParams.add("vorname");
		tcsearchParams.add("bundesland");
		tcsearchParams.add("kontonummer");
		tcsearchParams.add("spitzname");
		tcsearchParams.add("geschlecht");
		tcsearchParams.add("geburtsjahr");
		tcsearchParams.add("bankleitzahl");
		tcsearchParams.add("captureDate");
		tcsearchParams.add("addressZusatz");
		tcsearchParams.add("ort");
		tcsearchParams.add("followupdate");
		tcsearchParams.add("letteraddress_auto");
		tcsearchParams.add("kurzanrede");
		tcsearchParams.add("faxPrivat");
		tcsearchParams.add("postfachNr");
		tcsearchParams.add("titel");
		tcsearchParams.add("modificationDate");
		tcsearchParams.add("lettersalutation_auto");
		tcsearchParams.add("namenszusatz");
		tcsearchParams.add("geburtsdatum");
		tcsearchParams.add("telefonPrivat");
		tcsearchParams.add("herrnFrau");
		tcsearchParams.add("nameErweitert");
		tcsearchParams.add("bank");
		tcsearchParams.add("postfachNr2");
		tcsearchParams.add("nachname");
		tcsearchParams.add("plz");
		tcsearchParams.add("telefon");
		tcsearchParams.add("telefonMobilPrivat");
		tcsearchParams.add("institution");
		tcsearchParams.add("eMail");
		tcsearchParams.add("abteilung");
		tcsearchParams.add("land");
		tcsearchParams.add("lkz");
		tcsearchParams.add("adrNr");
		tcsearchParams.add("eMailPrivat");
		tcsearchParams.add("akadTitle");
		tcsearchParams.add("content_id");
		tcsearchParams.add("systemName");
	}
}
