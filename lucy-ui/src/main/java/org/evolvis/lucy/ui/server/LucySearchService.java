/*
 * lucy-ui,
 * lucy user interface
 * Copyright (C) 2000-2009 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'lucy-ui'
 * Signature of Elmar Geese, 21 April 2009
 * Elmar Geese, CEO tarent GmbH.
 */

package org.evolvis.lucy.ui.server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.evolvis.lucy.ui.server.LucyHit;
import org.evolvis.lucy.ui.server.LucyHits;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import de.tarent.octopus.client.OctopusConnection;
import de.tarent.octopus.client.OctopusConnectionFactory;
import de.tarent.octopus.client.OctopusResult;
import de.tarent.octopus.client.OctopusTask;
import de.tarent.octopus.client.remote.OctopusRemoteConnection;

/**
 * This Class is an proxy servlet to send an query to the indexer
 * 
 * @author mhelle tarent GmbH Bonn
 *
 */
public class LucySearchService extends HttpServlet {
	
	public int position;
    private static final long serialVersionUID = 1L;
    public static ArrayList<String> searchParams;
    JSONArray ja;

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	doPost(request, response);
    }
    
    @SuppressWarnings("unchecked")
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	response.setContentType("text/html; charset=UTF-8");
    	ja = new JSONArray();
    	
    	BufferedReader input = new BufferedReader(new InputStreamReader(request.getInputStream()));
    	
    	StringBuffer data = new StringBuffer();
    	String buf = input.readLine();
    	while(buf != null) {
    		data.append(buf);
    		buf = input.readLine();
    	}
    	JSONObject json = null;
    	JSONTokener tokenizer = null;
    	String mode = null;
    	String endpointAddress = null;
    	JSONArray activatedSystems = null;
    	
    	
		try {
			tokenizer = new JSONTokener(data.toString());
			json = new JSONObject(tokenizer);
			mode = json.getString("mode");
			endpointAddress = json.getString("endpoint");
		} catch (JSONException e1) {
			e1.printStackTrace();
		}
    	
    	if(mode.equals("search")){
	    	String q = null;
	    	
	    	position = 1;
	    
	    	try {
	    		tokenizer = new JSONTokener(data.toString());
				json = new JSONObject(tokenizer);
				q = json.getString("q");
				activatedSystems = json.getJSONArray("systems");
						
			} catch (JSONException e) {
				e.printStackTrace();
				System.out.println("[Exception] LucySearchService:JSONException] " + e.toString());
			}
			
		   LucyHits hits = new LucyHits();	   
		   try {
			   for (int i = 0; i < activatedSystems.length(); i++) {
				   hits = queryCall((String)activatedSystems.get(i), q, endpointAddress);
				   ja.put(0,"search");
//			    	position = 1;
		           fillJSONHit(hits, response, (String)activatedSystems.get(i));
			   }
		   } catch (JSONException e) {
				e.printStackTrace();
		   }
    	} else if (mode.equals("refresh")){
    		ArrayList<String> systems = refreshCall(endpointAddress);
    		try {
				ja.put(0,"refresh");
			} catch (JSONException e) {
				e.printStackTrace();
			}
    		fillJSONSystems(systems, response);
    	}
    	response.getWriter().print(ja);
    }
    /**
     * query call to the indexer
     * 
     * @param s
     * @param q
     * @param endpointAddress
     * @return hits
     */
    public LucyHits queryCall(String s, String q, String endpointAddress) {
    	
    	OctopusConnectionFactory octFact = OctopusConnectionFactory.getInstance();
    	
    	Map<String, String> ocConfig = new HashMap<String, String>();
        ocConfig.put(OctopusConnectionFactory.CONNECTION_TYPE_KEY, OctopusConnectionFactory.CONNECTION_TYPE_REMOTE);
        ocConfig.put(OctopusConnectionFactory.MODULE_KEY, "lucy-indexer");
        ocConfig.put(OctopusRemoteConnection.PARAM_SERVICE_URL, endpointAddress);
        ocConfig.put(OctopusRemoteConnection.AUTH_TYPE, OctopusRemoteConnection.AUTH_TYPE_SESSION);
        ocConfig.put(OctopusRemoteConnection.AUTO_LOGIN, "true");
        
        octFact.setConfiguration(endpointAddress, ocConfig);
        
    	OctopusConnection con = octFact.getConnection(endpointAddress);
    	OctopusTask query = con.getTask("query");
    	query.add("username", "");
    	query.add("password", "");
    	query.add("q", q);
    	query.add("s", s);
    	
    	OctopusResult res = query.invoke();
    	List<LucyHit> hitList = (List<LucyHit>) res.getData("hits");
    	LucyHits hits = new LucyHits();
    	hits.setHits(hitList);
    	
    	return hits;
    
    }
   
    public ArrayList<String> refreshCall(String endpointAddress){
    	OctopusConnectionFactory octFact = OctopusConnectionFactory.getInstance();
    	
    	Map<String, String> ocConfig = new HashMap<String, String>();
        ocConfig.put(OctopusConnectionFactory.CONNECTION_TYPE_KEY, OctopusConnectionFactory.CONNECTION_TYPE_REMOTE);
        ocConfig.put(OctopusConnectionFactory.MODULE_KEY, "lucy-indexer");
        ocConfig.put(OctopusRemoteConnection.PARAM_SERVICE_URL, endpointAddress);
        ocConfig.put(OctopusRemoteConnection.AUTH_TYPE, OctopusRemoteConnection.AUTH_TYPE_SESSION);
        ocConfig.put(OctopusRemoteConnection.AUTO_LOGIN, "true");
        
        octFact.setConfiguration(endpointAddress, ocConfig);
    	
    	OctopusConnection con = octFact.getConnection(endpointAddress);
    	OctopusTask query = con.getTask("getSystems");
    	query.add("username", "");
    	query.add("password", "");
    	
    	OctopusResult res = query.invoke();
    	List<String> systemList = (List<String>) res.getData("systems");
    	ArrayList<String> systems = new ArrayList<String>();
    	if(systemList != null){
	    	for(int i=0;i < systemList.size();i++){
	    		systems.add(systemList.get(i));
	    	}
    	}
    	return systems;
    }
    
    /**
     * This method fill the search results in an JSONArray
     * 
     * @param hits
     * @param response
     * @param systemName
     */
    private void fillJSONHit (LucyHits hits, HttpServletResponse response, String systemName){
    	if (hits != null){
	    	for(int i = 0;i < hits.size();i++){
	    		Map<String, String> hit = hits.getHits().get(i).getHit();
				try {
					ja.put(position, hit);
				} catch (JSONException e) {
					e.printStackTrace();
					System.out.println("[Exception] LucySearchService:JSONException] " + e.toString());
				}
	    		position++;
			}
    	}
//    	else
//    	{
//    		System.out.println("[NOT AVAILABLE]" + systemName);
//    		Map<String, String> hit = new LinkedHashMap<String, String>();
//    		hit.put("systemNotAvailable", systemName);
//    		try {
//				ja.put(position, hit);
//			} catch (JSONException e) {
//				e.printStackTrace();
//			}
//    		position++;
//    	}

    }
    
    /**
     * This mehtod fill the search systems in an JSONArray
     * 
     * @param systems
     * @param response
     */
    private void fillJSONSystems (ArrayList<String> systems, HttpServletResponse response){
    	position = 1;
    	if (systems != null){
	    	for(int i = 0;i < systems.size();i++){
	    		String systemName = systems.get(i);
				try {
					ja.put(position, systemName);
				} catch (JSONException e) {
					e.printStackTrace();
					System.out.println("[Exception] LucySearchService:JSONException] " + e.toString());
				}
				position++;
			}
    	}
    }
}