/*
 * lucy-ui,
 * lucy user interface
 * Copyright (C) 2000-2009 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'lucy-ui'
 * Signature of Elmar Geese, 21 April 2009
 * Elmar Geese, CEO tarent GmbH.
 */

package org.evolvis.lucy.ui.client.config;

import org.evolvis.lucy.ui.client.LucySearchModul;

import com.google.gwt.http.client.Request;
import com.google.gwt.http.client.RequestBuilder;
import com.google.gwt.http.client.RequestCallback;
import com.google.gwt.http.client.Response;
import com.google.gwt.user.client.Window;
/**
 * @author Frederic Eßer
 * 
 * This class is for the Request Calls
 *
 */
public class LucyRequestCallback implements RequestCallback {
	
	LucySearchModul lucySearchModul;
	
	/**
	 * @author Frederic Eßer
	 * 
	 * Constructor for the {@link LucyRequestCallback}
	 * 
	 * @param lucySearchModul
	 */
	LucyRequestCallback(LucySearchModul lucySearchModul)
	{
		this.lucySearchModul = lucySearchModul;
	}

	/**
	 * @author Frederic Eßer
	 * 
	 * This method handle the exception if {@link RequestBuilder} respond a Error
	 * 
	 * @see com.google.gwt.http.client.RequestCallback#onError(com.google.gwt.http.client.Request, java.lang.Throwable)
	 * 
	 * @param request, exception
	 */
	public void onError(Request request, Throwable exception)
	{
		requestFailed(exception);
	}

	/**
	 * @author Frederic Eßer
	 * 
	 * This method handle the next step if {@link RequestBuilder} received a respond
	 * 
	 * @see com.google.gwt.http.client.RequestCallback#onResponseReceived(com.google.gwt.http.client.Request, com.google.gwt.http.client.Response)
	 * 
	 * @param request, response
	 */
	public void onResponseReceived(Request request, Response response)
	{
		this.lucySearchModul.setTargetAddress(response.getText());
	}
	
	/**
	 * @author Frederic Eßer
	 * 
	 * This method handle the next step if {@link RequestBuilder}s request failed
	 * 
	 * @param exception
	 */
	public void requestFailed(Throwable exception)
	{
		Window.alert("Request failed. Error was: " + exception.getMessage());
	}

}
