/*
 * lucy-ui,
 * lucy user interface
 * Copyright (C) 2000-2009 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'lucy-ui'
 * Signature of Elmar Geese, 21 April 2009
 * Elmar Geese, CEO tarent GmbH.
 */

package org.evolvis.lucy.ui.client;

import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.ClickListener;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.FormHandler;
import com.google.gwt.user.client.ui.FormPanel;
import com.google.gwt.user.client.ui.FormSubmitCompleteEvent;
import com.google.gwt.user.client.ui.FormSubmitEvent;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.Hidden;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.MultiWordSuggestOracle;
import com.google.gwt.user.client.ui.SuggestBox;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;

/**
 * This is an abstract class for all lucy-ui moduls
 *   
 * @author Viktor Hamm tarent GmbH Bonn
 *
 */
@Deprecated
public abstract class LucyModule {

	/**
	 * The Default Constructor for this class
	 */
	public LucyModule() {
	}

	/**
	 * The HTML Widgets for the search fields
	 */
	public HTML includeExtField;
	public HTML lucydesc;

	/**
	 * Panels for the Layout of the LucyModules
	 */
	public HorizontalPanel mainSearchField;
	public VerticalPanel extSearchField;
	public VerticalPanel inputField;
	public VerticalPanel resultField;
	public FormPanel form;
	public FlowPanel systemCheckBox;

	/**
	 * GWT Widgets to use
	 */
	public CheckBox systemTypes = new CheckBox();
	public SuggestBox input;
	public Label includeField;
	public HTML lucySearchForText = new HTML();

	/**
	 * Temporary variable
	 */
	public String searchText;

	/**
	 * This is a test String with search systems because at the moment
	 * we can't automatically includes all regist systems from the 
	 * Lucy Indexer
	 */
	public static final String[] SYSTEM_TYPES = new String[] { "Evolvis",
			"Intranet Wiki", "Infrastruktur Wiki", "Keyaccount Wiki",
			"Tarent Contact" };

	/**
	 * Test String to demonstrate the autocomplete Method by SuggestBox and
	 * MultiWordSuggestBoxOracle
	 */
	private static final String[] datasearch = new String[] { "apache",
			"alternative", "bug", "border", "bottom-style", "cebit", "control",
			"description", "destructor", "develop", "engine", "exception",
			"entity", "evolvis", "freedom", "function", "GWT", "google",
			"Hybernate", "hitrate", "iframe", "infrastruktur wiki", "JDK",
			"Java", "Jalimo", "Kaliko", "KDE", "lucy", "Linux day", "mobile",
			"maemo", "maven", "N800", "Nokia", "Octopus", "object-oriented",
			"priority", "project", "Query", "QT", "reload", "release",
			"static", "swing", "tomcat", "tactic", "tarent", "universe",
			"velocity", "veraweb", "welcome", "xana", "Yggdrasil-Linux",
			"certification" };

	/**
	 * This class is the Clicklistener for all system check boxes.
	 * The class will be need to develop an dynamic search result
	 * filtering by activation/deactivation a system check box.
	 * 
	 * @author Viktor Hamm tarent GmbH Bonn
	 *
	 */
	public class SystemCheckBoxFilter implements ClickListener {

		public CheckBox systems;

		public SystemCheckBoxFilter(CheckBox systems) {

			this.systems = systems;
		}

		public void onClick(Widget sender) {
			System.out.println("test");
		}
	}

	/**
	 * This method builds the main Lucy description field
	 * 
	 * @return HTML lucydesc
	 */
	public HTML buildDescField() {

		lucydesc = new HTML(
				"<h1 lang=\"en\">About lucy</h1>"
						+ "<p lang=\"en\"> "
						+ "lucy will integrate different kinds of informationsystem contents in one big index and allow "
						+ "access through a single search engine. Configuration with xml files and Firefox search Integration is possible. "
						+ "lucy is based on the java library Lucene. " + "</p>");

		return lucydesc;
	}

	/**
	 * This method build a FormPanel with the normal search input field
	 * and all automatically generated search system check boxes.
	 * 
	 * TODO:
	 * -modify formhandler
	 * -after an click on submit open next page - done
	 * 
	 * @return FormPanel form
	 */
	public FormPanel buildMainFormField() {

		form = new FormPanel();
		final Hidden data = new Hidden("hiddendatafield");

		VerticalPanel panel = new VerticalPanel();

		form.setEncoding(FormPanel.ENCODING_MULTIPART);
		form.setMethod(FormPanel.METHOD_GET);
		form.setAction("/org.evolvis.lucy.ui.LucySearchModul/lucy-index.html");

		Button submit = new Button("Submit", new ClickListener() {
			/** 
			 * @see com.google.gwt.user.client.ui.ClickListener#onClick(com.google.gwt.user.client.ui.Widget)
			 */
			public void onClick(Widget sender) {
				form.submit();
			}
		});

		submit.setStyleName("submitbutton");

		panel.add(buildSearchField());
		panel.add(generateSystems());
		panel.add(submit);
		panel.add(data);

		form.add(panel);

		form.addFormHandler(new FormHandler() {

			/**
			 * @see com.google.gwt.user.client.ui.FormHandler#onSubmit(com.google.gwt.user.client.ui.FormSubmitEvent)
			 */
			public void onSubmit(FormSubmitEvent event) {
				// TODO Auto-generated method stub

				if (input.getText().length() == 0) {
					input.setText("input field must not be empty");
					event.setCancelled(true);
				}
			}

			/**
			 * @see com.google.gwt.user.client.ui.FormHandler#onSubmitComplete(com.google.gwt.user.client.ui.FormSubmitCompleteEvent)
			 * 
			 * TODO: Bug -> onSubmitComplete() don't fire an event
			 */
			public void onSubmitComplete(FormSubmitCompleteEvent event) {
				// TODO Auto-generated method stub
				
				data.setValue(input.getText().toString());
				searchText = data.getValue();
				
				lucySearchForText.setHTML("<p>You searched for <span class=\"highlight\">"
								+ searchText + "</span>.</p>");

			}
		});

		return form;
	}

	/**
	 * This is the MultiWordSuggestOracle method. It
	 * includes all Data from the test String datasearch
	 * in the Popup from the SuggestBox. The User types an
	 * word, then will be the popup shown with an suggestion
	 * 
	 * @return MultiWordSuggestOracle datainput
	 */
	public MultiWordSuggestOracle createDataInput() {

		MultiWordSuggestOracle dataInput = new MultiWordSuggestOracle();

		for (int i = 0; i < datasearch.length; i++)
			dataInput.add(datasearch[i]);

		return dataInput;
	}

	/**
	 * This method build the normal search field with an HorizontalPanel for the
	 * end user. Also its includes an autocomplete method by the SuggestBox
	 * Widget
	 * 
	 * @return HorizontalPanel mainSearchField
	 */
	public HorizontalPanel buildSearchField() {

		TextBox inputTextBox = new TextBox();
		inputTextBox.setName("inputfield");

		mainSearchField = new HorizontalPanel();
		input = new SuggestBox(createDataInput(), inputTextBox);

		includeField = new Label("search for: ");

		mainSearchField.add(includeField);
		mainSearchField.add(input);

		mainSearchField.setStyleName("searchfield");
		mainSearchField.setSpacing(10);

		return mainSearchField;
	}

	/**
	 * This method builds all regist systems from the index, automatically
	 * but at the moment only with test data.
	 * 
	 * @return FlowPanel systemsOrder
	 */
	public FlowPanel generateSystems() {

		systemCheckBox = new FlowPanel();
		systemCheckBox.setStyleName("checkboxfield");

		for (int i = 0; i < SYSTEM_TYPES.length; i++) {
			systemTypes = new CheckBox(SYSTEM_TYPES[i]);
			systemTypes.setName(SYSTEM_TYPES[i]);
			systemTypes.setChecked(true);
			systemTypes.addClickListener(new SystemCheckBoxFilter(systemTypes));
			systemTypes.addStyleName("checkbox");
			systemCheckBox.add(systemTypes);
		}

		return systemCheckBox;
	}

}