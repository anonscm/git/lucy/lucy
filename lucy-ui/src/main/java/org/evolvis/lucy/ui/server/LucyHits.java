/*
 * lucy-ui,
 * lucy user interface
 * Copyright (C) 2000-2009 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'lucy-ui'
 * Signature of Elmar Geese, 21 April 2009
 * Elmar Geese, CEO tarent GmbH.
 */

package org.evolvis.lucy.ui.server;

import java.util.ArrayList;
import java.util.List;
/**
 * Bean for search results at the server side
 * 
 * @author Viktor Hamm tarent GmbH Bonn
 *
 */
public class LucyHits {
	
	List<LucyHit> hits = new ArrayList<LucyHit>();

	public List<LucyHit> getHits() {
		return hits;
	}

	public void setHits(List<LucyHit> hits) {
		this.hits = hits;
	}
	public int size(){
		return this.hits.size();
	}

}
