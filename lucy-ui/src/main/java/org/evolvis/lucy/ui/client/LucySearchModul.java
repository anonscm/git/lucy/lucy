/*
 * lucy-ui,
 * lucy user interface
 * Copyright (C) 2000-2009 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'lucy-ui'
 * Signature of Elmar Geese, 21 April 2009
 * Elmar Geese, CEO tarent GmbH.
 */

package org.evolvis.lucy.ui.client;

import org.evolvis.lucy.ui.client.config.LucyConfig;
import org.evolvis.lucy.ui.client.i18n.InternationalizationConstants;
import org.evolvis.lucy.ui.client.i18n.impl.LanguageField;
import org.evolvis.lucy.ui.client.ui.Content;
import org.evolvis.lucy.ui.client.ui.UIComponents;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.core.client.GWT;


import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.ClickListener;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.xml.client.Document;
import com.google.gwt.xml.client.Node;
import com.google.gwt.xml.client.XMLParser;

/**
 * This Class is the Search Modul for the lucy-index.html 
 * 
 * TODO:
 * - 10 til 20 search results per seite - not started
 * - drag and drop compatibility for each important widget - not started
 * - drag and drop compatibility saves in a cookie - not started
 * - sortable search results with drag and drop. Search results must be written in a table - not started
 *   => filtering by date, datatype, hitrate..
 *   
 * @author Viktor Hamm tarent GmbH Bonn
 *
 */
public class LucySearchModul implements EntryPoint {
	
	UIComponents components = new UIComponents();
	Content uicontent = new Content();
	
//	public static final String DEFAULT_THEME_STYLE = "theme-default";
//	public static final String BLACK_THEME_STYLE = "black-theme";
		
	public static String targetAddress = "";

	/**
	 * Internationalization in GWT
	 */
	static InternationalizationConstants constants = (InternationalizationConstants) GWT
			.create(InternationalizationConstants.class);

	public LanguageField language;
	
	/**
	 * The Default Constructor for this class
	 */
	public LucySearchModul() {
		language = new LanguageField();
		Content.initInputField();
	}
	/**
	 * @author Frederic Eßer
	 * 
	 * This method parse the content of the lucy-uiconfig.xml and filter
	 * the targetAddress
	 * 
	 * @param fileContent
	 */
	public void setTargetAddress(String fileContent)
	{
		Document doc = XMLParser.parse(fileContent);
		Node address = doc.getDocumentElement().getLastChild();
		LucySearchModul.targetAddress = address.getFirstChild().toString();
	}

	/**
	 * @author Frederic Eßer
	 * 
	 * method to return the targetAddress
	 * 
	 * @return targetAddress
	 */
	public static String getTargetAddress()
	{
		return targetAddress;
	}
	
	/**
	 * This is the entry point method. You need it, when you will
	 * use a Module in a HTML File
	 * 
	 * @see com.google.gwt.core.client.EntryPoint#onModuleLoad()
	 */
	public void onModuleLoad() {
	
		HTML headline = new HTML("<h1>" + constants.lucy_engine() + "</h1>");

//		RootPanel.get().setStyleName(DEFAULT_THEME_STYLE);
		/**
		 * Insert a HTML Widget with text.
		 */
		RootPanel.get("lucyheadline").add(headline);

		/**
		 * the main field with an search box and extended field with 
		 * all generated check boxes
		 */
		RootPanel.get("mainform").add(uicontent.buildMainSearchField());
		RootPanel.get("checkboxfield").add(components.generateSystems());
				
		/**
		 * HTML Widgets for the description text and the search room text
		 * with an link to set the extended search field on visible because
		 * this field is invisible while the first start of the result page
		 */
		RootPanel.get("searchroom").add(components.aboutLucyText());
		RootPanel.get("lucydesctext").add(components.buildDescField());
		
		/**
		 * A Hidden widget to save the previous suggest list
		 */
		RootPanel.get("hidden").add(uicontent.oracleHiddenField());
		RootPanel.get("userinput").add(uicontent.userInputHiddenField());
		
		/**
		 * add an language field to the user interface
		 */
		RootPanel.get("language").add(language.buildLanguageField());
		
//		Button button = new Button("click me");
//		button.addClickListener(new ClickListener() {
//
//			public void onClick(Widget sender) {
//				// TODO Auto-generated method stub
//				String classname = RootPanel.get().getStyleName();
//				
//				if(DEFAULT_THEME_STYLE.equalsIgnoreCase(classname)) {
//					RootPanel.get().setStyleName(BLACK_THEME_STYLE);
//				}else {
//					RootPanel.get().setStyleName(DEFAULT_THEME_STYLE);
//				}
//			}
//			
//		});
//		
//		RootPanel.get("button").add(button);
		
		/**
		 * get the targetAddress for lucy-indexer communication
		 */
		LucyConfig.getXMLContent(this);
	}
}
/* dynamic checkboxes
 * 
		LucySearchModule
		public void refreshSystemCheckBoxes(){
		JSONObject o = new JSONObject();
		RequestBuilder rb = null;
		o.put("endpoint", new JSONString(LucySearchModul.getTargetAddress()));
		o.put("mode", new JSONString("refresh"));
			
		rb = new RequestBuilder(RequestBuilder.POST, GWT.getModuleBaseURL() + "/LucySearchService");
		try {
			rb.sendRequest(o.toString(), new SearchResultHandler());
		} catch (RequestException e) {
			e.printStackTrace();
		}	
		
	}

 */