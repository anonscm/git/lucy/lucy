/*
 * lucy-ui,
 * lucy user interface
 * Copyright (C) 2000-2009 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'lucy-ui'
 * Signature of Elmar Geese, 21 April 2009
 * Elmar Geese, CEO tarent GmbH.
 */

/**
 * LucyIndexerServiceLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package org.evolvis.lucy.ui.server;

public class LucyIndexerServiceLocator extends org.apache.axis.client.Service implements org.evolvis.lucy.ui.server.LucyIndexerService {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public LucyIndexerServiceLocator() {
    }


    public LucyIndexerServiceLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public LucyIndexerServiceLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for LucyIndexerPort
    private java.lang.String LucyIndexerPort_address = "http://localhost:8080/octopus";

    public java.lang.String getLucyIndexerPortAddress() {
        return LucyIndexerPort_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String LucyIndexerPortWSDDServiceName = "lucy-indexerPort";

    public java.lang.String getLucyIndexerPortWSDDServiceName() {
        return LucyIndexerPortWSDDServiceName;
    }

    public void setLucyIndexerPortWSDDServiceName(java.lang.String name) {
        LucyIndexerPortWSDDServiceName = name;
    }

    public org.evolvis.lucy.ui.server.LucyIndexerPortType getLucyIndexerPort() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(LucyIndexerPort_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getLucyIndexerPort(endpoint);
    }

    public org.evolvis.lucy.ui.server.LucyIndexerPortType getLucyIndexerPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
        	org.evolvis.lucy.ui.server.LucyIndexerBindingStub _stub = new org.evolvis.lucy.ui.server.LucyIndexerBindingStub(portAddress, this);
            _stub.setPortName(getLucyIndexerPortWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setLucyIndexerPortEndpointAddress(java.lang.String address) {
        LucyIndexerPort_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (org.evolvis.lucy.ui.server.LucyIndexerPortType.class.isAssignableFrom(serviceEndpointInterface)) {
            	org.evolvis.lucy.ui.server.LucyIndexerBindingStub _stub = new org.evolvis.lucy.ui.server.LucyIndexerBindingStub(new java.net.URL(LucyIndexerPort_address), this);
                _stub.setPortName(getLucyIndexerPortWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("lucy-indexerPort".equals(inputPortName)) {
            return getLucyIndexerPort();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://evolvis.org/lucy/services/", "lucy-indexerService");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://evolvis.org/lucy/services/", "lucy-indexerPort"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("LucyIndexerPort".equals(portName)) {
            setLucyIndexerPortEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
