package org.evolvis.lucy.test;

import java.io.File;
//import java.io.FileInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.List;

import org.evolvis.lucy.config.IndexerSystemConfig;
import org.evolvis.lucy.config.SystemConfig;
import org.evolvis.lucy.config.IndexerConfig;
import org.evolvis.lucy.config.XmlConfigParser;


public class TestXmlParser {
	
	public static void main(String[] args) {
		
		XmlConfigParser cp = new XmlConfigParser();
		IndexerConfig config = new IndexerConfig();		
		try {
			
			config = cp.deserialize(new FileInputStream(new File("../../../main/resources/lucyconfig.xml")));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		IndexerSystemConfig fact = config.getSystemFactory();
		List<SystemConfig> systems = fact.getSystems();
		for(int i = 0; i < systems.size(); i++){
			System.out.println(systems.get(i).toString());
		}
		
	}

}
