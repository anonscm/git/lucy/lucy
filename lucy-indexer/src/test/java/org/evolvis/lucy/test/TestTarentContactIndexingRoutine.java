package org.evolvis.lucy.test;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.sql.SQLException;
import java.util.List;

import org.evolvis.lucy.config.IndexerConfig;
import org.evolvis.lucy.config.IndexerSystemConfig;
import org.evolvis.lucy.config.SystemConfig;
import org.evolvis.lucy.config.XmlConfigParser;
import org.evolvis.lucy.dataaccess.LucyDataAccessException;
import org.evolvis.lucy.indexer.LucyFactory;
import org.evolvis.lucy.indexer.LucyRegistry;

import de.tarent.octopus.server.Context;

public class TestTarentContactIndexingRoutine {
	
	public static void main(String[] args) throws SQLException, LucyDataAccessException {
		
		List<SystemConfig> systems = getSystemsFromXMLConfigFile();	
		
		for (SystemConfig systemConf : systems) {
			
			File indexPath = new File(systemConf.getIndexPath());			
			if(!indexPath.exists()){
				LucyFactory lucyFactory = LucyFactory.create(systemConf);
				System.out.println("[lucy-indexer] Indexing : " + systemConf.getSystemName() + "...");
				lucyFactory.getIndexBuilder().rebuild();
				System.out.println("[lucy-indexer] Puting " + systemConf.getSystemName()+ " into registry.");
				LucyRegistry.getInstance().put(systemConf.getSystemName(), lucyFactory);
			} else {
				System.out.println("[lucy-indexer] " + systemConf.getSystemName() + " had allready been indexed.");
				System.out.println("[lucy-indexer] Puting " +  systemConf.getSystemName() + " into registry.");
				LucyFactory lucyFactory = LucyFactory.create(systemConf);
				LucyRegistry.getInstance().put(systemConf.getSystemName(), lucyFactory);
			}
			
		}
		
	}

	
	public static List<SystemConfig> getSystemsFromXMLConfigFile() {
		XmlConfigParser cp = new XmlConfigParser();
		IndexerConfig config = new IndexerConfig();		
		try {
			config = cp.deserialize(new FileInputStream(new File("src/main/webapp/lucyconfig.xml")));
		} catch (FileNotFoundException e) {
			System.out.println("[lucy-indexer] Could not get config file for path: " + Context.getActive().moduleRootPath().getParent()+"/lucyconfig.xml");
			e.printStackTrace();
		}
		IndexerSystemConfig fact = config.getSystemFactory();
		List<SystemConfig> systems = fact.getSystems();
		
		return systems;
	
	}
}
