package org.evolvis.lucy.indexer.mediawiki;

import org.evolvis.lucy.config.SystemConfig;
import org.evolvis.lucy.indexer.LucySearcher;

public class MediawikiSearcher extends LucySearcher {

	public MediawikiSearcher(SystemConfig systemConfig) {
		super(systemConfig);
	}
}
