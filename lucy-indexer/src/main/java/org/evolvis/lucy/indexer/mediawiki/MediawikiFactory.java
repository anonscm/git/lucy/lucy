package org.evolvis.lucy.indexer.mediawiki;

import org.evolvis.lucy.config.SystemConfig;
import org.evolvis.lucy.indexer.LucyDocumentBuilder;
import org.evolvis.lucy.indexer.LucyFactory;
import org.evolvis.lucy.indexer.LucyIndexBuilder;
import org.evolvis.lucy.indexer.LucyIndexProvider;
import org.evolvis.lucy.indexer.LucySearcher;

/**
 * @author Marco Hellenthal tarent GmbH Bonn
 * 
 * MediawikiFactory factory modell
 */
public class MediawikiFactory extends LucyFactory {
	
	protected static LucyDocumentBuilder<?> documentBuilder;
	protected static LucyIndexBuilder<?> indexBuilder;
	protected static LucyIndexProvider indexProvider;
	protected static LucySearcher searcher;
		
	public MediawikiFactory(SystemConfig systemConfig) {
		super(systemConfig);
	}

	public LucyDocumentBuilder<?> getDocumentBuilder() {
		if(documentBuilder == null){
			documentBuilder = new
			MediawikiDocumentBuilder(systemConfig);
		}
		return documentBuilder;
	}

	public LucyIndexBuilder<?> getIndexBuilder() {
		if(indexBuilder == null) {
			indexBuilder = new MediawikiIndexBuilder(systemConfig);
		}
		return indexBuilder;
	}

	@Override
	public LucyIndexProvider getIndexProvider() {
		if(indexProvider == null) { 
			indexProvider = new MediaWikiIndexProvider(systemConfig);
		}
		return indexProvider;	
	}

	@Override
	public LucySearcher getSearcher() {
		if(searcher == null) { 
			searcher = new MediawikiSearcher(systemConfig);
		}
		return searcher;	
	}
}
