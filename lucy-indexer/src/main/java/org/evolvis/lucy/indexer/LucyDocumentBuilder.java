package org.evolvis.lucy.indexer;

import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.evolvis.lucy.config.SystemConfig;

public abstract class LucyDocumentBuilder<T> {
	//documentbuilder offers methods for puting severaly fields into the document
	protected SystemConfig systemConfig;
	protected static final float BOOST_TITLE = 3.0f;
	protected static final float BOOST_DATA = 2.0f;
	
	public LucyDocumentBuilder(SystemConfig systemConfig) {
		this.systemConfig = systemConfig;
	}

	protected static String translateUmlauts(String in){
        return in.replaceAll("Ã¤", "ä")
            	 .replaceAll("Ã¶", "ö")
            	 .replaceAll("Ã¼", "ü")
            	 .replaceAll("Ã", "Ä")
            	 .replaceAll("Ã", "Ö")
            	 .replaceAll("Ã", "Ü")
            	 .replaceAll("Ã", "ß");
    }
	
	protected void addSystemName(String data, Document doc){
		String sys = "";
		if(data != null){
			sys = data;
		} else {
			sys = "null";
		}
		
		Field sysN = new Field( "systemName",
				translateUmlauts(sys),
				 Field.Store.YES,/*Store the original field value in the index.
				  				   This is useful for short texts like a document's title which should
				  				   be displayed with the results */
				 Field.Index.TOKENIZED );/*Index the field's value so it can be searched
					  	  				   This is useful for common text.*/	
		sysN.setBoost(BOOST_TITLE);/*Boost searchresults found in this field*/
		doc.add( sysN );
	}
	
	public void addTitle(String data, Document doc ){
		String tit = "";
		if(data != null){
			tit = data;
		} else {
			tit = "null";
		}
		
		Field title = new Field( "title",
				translateUmlauts(tit),
                Field.Store.YES,
                Field.Index.TOKENIZED );
		title.setBoost(BOOST_TITLE);
		doc.add( title );
	}
	protected void addContentId(String data, Document doc){
		String cont_id = "";
		if(data != null){
			cont_id = data;
		} else {
			cont_id = "null";
		}
		
		Field content_id = new Field( "content_id",
				cont_id,
                Field.Store.YES,
                Field.Index.UN_TOKENIZED );
		doc.add( content_id );		
	}
	
}
