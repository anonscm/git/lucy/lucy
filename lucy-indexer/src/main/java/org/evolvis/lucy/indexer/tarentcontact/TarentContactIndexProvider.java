package org.evolvis.lucy.indexer.tarentcontact;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.lucene.document.Document;
import org.apache.lucene.search.Hits;
import org.evolvis.lucy.config.SystemConfig;
import org.evolvis.lucy.hits.LucyHit;
import org.evolvis.lucy.hits.LucyHits;
import org.evolvis.lucy.indexer.LucyIndexProvider;
import de.tarent.commons.logging.LogFactory;

public class TarentContactIndexProvider extends LucyIndexProvider  {

	public TarentContactIndexProvider(SystemConfig systemConfig) {
		super(systemConfig);
	}
	
	public static ArrayList<String> searchParams;
	
	public void initSearchParams() {
		
		searchParams = new ArrayList<String>();
		searchParams.add("anrede");
		searchParams.add("letteraddress");
		searchParams.add("institutionErweitert");
		searchParams.add("strasse");
		searchParams.add("position");
		searchParams.add("hausNr");
		searchParams.add("fk_user_followup");
		searchParams.add("followup");
		searchParams.add("homepage");
		searchParams.add("fax");
		searchParams.add("vorname");
		searchParams.add("bundesland");
		searchParams.add("kontonummer");
		searchParams.add("spitzname");
		searchParams.add("geschlecht");
		searchParams.add("geburtsjahr");
		searchParams.add("bankleitzahl");
		searchParams.add("captureDate");
		searchParams.add("addressZusatz");
		searchParams.add("ort");
		searchParams.add("followupdate");
		searchParams.add("letteraddress_auto");
		searchParams.add("kurzanrede");
		searchParams.add("faxPrivat");
		searchParams.add("postfachNr");
		searchParams.add("titel");
		searchParams.add("modificationDate");
		searchParams.add("lettersalutation_auto");
		searchParams.add("namenszusatz");
		searchParams.add("geburtsdatum");
		searchParams.add("telefonPrivat");
		searchParams.add("herrnFrau");
		searchParams.add("nameErweitert");
		searchParams.add("bank");
		searchParams.add("postfachNr2");
		searchParams.add("nachname");
		searchParams.add("plz");
		searchParams.add("telefon");
		searchParams.add("telefonMobilPrivat");
		searchParams.add("institution");
		searchParams.add("eMail");
		searchParams.add("abteilung");
		searchParams.add("land");
		searchParams.add("lkz");
		searchParams.add("adrNr");
		searchParams.add("eMailPrivat");
		searchParams.add("akadTitle");
		searchParams.add("content_id");
		searchParams.add("systemName");	
	}
	
	/**
	 * @author Marco Hellenthal tarent GmbH Bonn
	 * 
	 * An address has more fields to be searched,so the routine
	 * for searching over an the index has to be overwritten to get
	 * optimal results.Because that tarent contact addresses are no typical textdocuments
	 * it is essential to build a bigger LucyHit with more fields
	 */ 
	@Override
	public LucyHits executeQuery(String q) {	
		
		File anIndexDir = new File(systemConfig.getIndexPath());		
		LucyHits hits = new LucyHits();		
		TarentContactSearcher s = new TarentContactSearcher(systemConfig);		
		List<LucyHit> hitList = new ArrayList<LucyHit>();		
		Hits h = s.executeQuery(anIndexDir, q);		
		initSearchParams();
		
		for (int i = 0; i < h.length(); i++){
			try {
				Document doc = h.doc(i);
				Map<String, String> tmp = new LinkedHashMap<String, String>();
				LucyHit hit = new LucyHit();
				for(int e = 0;e<searchParams.size();e++){
					if(searchParams.get(e).equals("systemName")){
						tmp.put("name", doc.get(searchParams.get(e)));
					} else {
						tmp.put(searchParams.get(e), doc.get(searchParams.get(e)));
					}
				}
				hit.setHit(tmp);
				hitList.add(hit);		
				
			} catch (IOException e) {
				LogFactory.log("[lucy-indexer] Could not create document.", e);
			}
		}
		hits.addAll(hitList);
		
		return hits;
	}

}
