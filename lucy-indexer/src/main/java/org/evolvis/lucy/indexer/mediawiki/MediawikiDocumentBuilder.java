package org.evolvis.lucy.indexer.mediawiki;

import java.util.Map;

import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.evolvis.lucy.config.SystemConfig;
import org.evolvis.lucy.indexer.LucyDocumentBuilder;

public class MediawikiDocumentBuilder extends LucyDocumentBuilder<Map<String, String>> {

	public MediawikiDocumentBuilder(SystemConfig systemConfig) {
		super(systemConfig);
	}
	/**
	 * @author Marco Hellenthal tarent GmbH Bonn
	 * 
	 * protected Document createDocument(Map<String,String> content) adds
	 * the required fields into the document which should be written in the index
	 */
	protected Document createDocument(Map<String,String> content) {
			
		Document doc = new Document();
			
		addTitle(content.get( "page_title" ), doc);				
		addContentType(content.get( "page_namespace" ), doc);
		addContentId(content.get( "si_page" ), doc);
		addContent(content.get( "si_text" ), doc);
		addURL(systemConfig.getBasePath(), content.get("page_namespace"), doc);
		addSystemName(systemConfig.getSystemName(), doc);
		
		return doc;
	}
	/**
	 * @author Marco Hellenthal tarent GmbH Bonn
	 * 
	 * adds a new field into the document
	 */
	protected void addURL(String basePath,String contentType,Document doc) {
		String path = "";
		if (contentType.equals("0"))
			path = basePath + "index.php/" + doc.get("title").replace("-", "");
		else if(contentType.equals("2"))
			path = basePath + "index.php/Benutzer:"+ doc.get("title").replace("-", "");
		else if(contentType.equals("4"))
			path = basePath + "index.php/Keyaccount_Wiki:" + doc.get("title").replace("-", "");
		else if(contentType.equals("6"))
			path = basePath + "index.php/Bild:"+ doc.get("title").replace("-", "");
  	 	else if(contentType.equals("8"))
  	 		path = basePath + "index.php/Media_Wiki:"+ doc.get("title").replace("-", "");
  	 	else if(contentType.equals("10"))
  	 		path = basePath + "index.php/Vorlage:"+ doc.get("title").replace("-", "");
  	 	else if(contentType.equals("12"))
  	  		path = basePath + "index.php/Anleitung:"+ doc.get("title").replace("-", "");
  	 	else if(contentType.equals("14"))
  	 		path = basePath + "index.php/Kategorie:"+ doc.get("title").replace("-", "");
  	  	else {
  	 		path = contentType;
  	 	}
		Field URL = new Field( "url",
				path,
                Field.Store.YES,
                Field.Index.TOKENIZED );
		doc.add( URL );
		
	}
	/**
	 * @author Marco Hellenthal tarent GmbH Bonn
	 * 
	 * adds a new field into the document
	 */
	protected void addContent(String data, Document doc){
		String dat = "";
		if(data != null){
			dat = data;
		} else {
			dat = "null";
		}
		Field dataField = new Field( "data",
				translateUmlauts(dat),
                Field.Store.YES,
                Field.Index.TOKENIZED );
		dataField.setBoost(BOOST_DATA);
		doc.add( dataField );		
	}
	/**
	 * @author Marco Hellenthal tarent GmbH Bonn
	 * 
	 * adds a new field into the document
	 */
	protected void addContentType(String data, Document doc){
		String contentType = "";
		if(data != null){
			contentType = data;
		} else {
			contentType = "null";
		}
		Field conType = new Field( "contentType",
				contentType,
                Field.Store.YES,
                Field.Index.UN_TOKENIZED);
		doc.add( conType );				
	}
}
