package org.evolvis.lucy.indexer;

import java.io.File;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.lucene.analysis.PerFieldAnalyzerWrapper;
import org.apache.lucene.analysis.de.GermanAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.Term;
import org.evolvis.lucy.config.SystemConfig;
import org.evolvis.lucy.dataaccess.LucyDataAccessException;

import de.tarent.commons.logging.LogFactory;

//Here were using generics to reserve the type of content for the indexing routine
public abstract class LucyIndexBuilder<T> {
	
	protected SystemConfig systemConfig;
	
	protected Document doc;
	protected static PerFieldAnalyzerWrapper analyzer;
	public abstract void createIndex() throws LucyDataAccessException;
	/**
	 * @author Marco Hellenthal tarent GmbH Bonn
	 * 
	 * public PerFieldAnalyzerWrapper getAnalyzer() returns
	 * the required analyzer for the system
	 */
	public PerFieldAnalyzerWrapper getAnalyzer() {
		if (analyzer == null) {
			analyzer = new PerFieldAnalyzerWrapper( new GermanAnalyzer() );
		    analyzer.addAnalyzer( "title", new GermanAnalyzer() );
		    analyzer.addAnalyzer( "data", new GermanAnalyzer() );
		    return analyzer;
		}
		return analyzer;
	}
	
	protected abstract void indexContent(T content, File indexPath);
	

	public LucyIndexBuilder(SystemConfig systemConfig) {
		this.systemConfig = systemConfig;
	}
	
	/**
	 * @author Marco Hellenthal tarent GmbH Bonn
	 * 
	 * protected void checkDoc(File path,Document doc) checks if the given index is allready existing
	 */
	protected void checkDoc(File path,Document doc) throws IOException {
		IndexReader reader = null;
		try
        {	
			reader = IndexReader.open(path);
			reader.deleteDocuments(new Term("content_id", doc.get("content_id")));
			reader.close();
			/*If no IOException is thrown, were only comparing the documents id's, which should be indexed
			 * with all documents located to the index. We're doing this for bewaring redundance in the index */
        } catch(IOException e){
        	//if IOException is thrown, we are sure, that we have to build the index first time.
           LogFactory.log("[lucy-indexer] No Index heretofore existing! Will be builded...", null);
           LogFactory.log("[lucy-indexer] Building index for " + doc.get("systemName") + "...", null);      
           IndexWriter writer = getWriter(path);
		   writer.addDocument( doc );
		   writer.close();
	    }
	}
	/**
	 * @author Marco Hellenthal tarent GmbH Bonn
	 * 
	 * protected IndexWriter getWriter( File path) returns
	 * the used writer for this system
	 */
	protected IndexWriter getWriter( File path) throws IOException {
		IndexWriter writer = null;
		try
		{
	        writer = new IndexWriter( path, getAnalyzer(), false );
	    } catch (Exception e)
	    {
	        writer = new IndexWriter( path, getAnalyzer(), true );
	    }
	    return writer;
	}
	/**
	 * @author Marco Hellenthal tarent GmbH Bonn
	 * 
	 * protected static final List<Map<String, String>> toList(ResultSet rs, List<String> wantedColumnNames)
	 * wraps the given db resultset to List<Map<String, String>>
	 */ 
	protected static final List<Map<String, String>> toList(ResultSet rs, List<String> wantedColumnNames) throws SQLException
    {
        List<Map<String, String>> rows = new ArrayList<Map<String, String>>();
 
        int numWantedColumns = wantedColumnNames.size();
        while (rs.next())
        {
            Map<String, String> row = new LinkedHashMap<String, String>();
 
            for (int i = 0; i < numWantedColumns; ++i)
            {
                String columnName   = (String)wantedColumnNames.get(i);
                String value = rs.getString(columnName);
                row.put(columnName, value);
            } 
            rows.add(row);
        } 
        return rows;
    }
	/**
	 * @author Marco Hellenthal tarent GmbH Bonn
	 * 
	 * rebuilds the index
	 */ 
	public void rebuild()throws LucyDataAccessException {
		createIndex();
		try {
			IndexWriter writer = getWriter(new File(systemConfig.getIndexPath()));
			writer.optimize();//Merges all segments together into a single segment, optimizing an index for search.
			writer.close();				
		} catch (IOException e) {
			e.printStackTrace();
		}		
	}
}
