package org.evolvis.lucy.indexer;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

import org.evolvis.lucy.config.SystemConfig;

import de.tarent.commons.logging.LogFactory;

/**
 * @author Marco Hellenthal tarent GmbH Bonn
 * 
 * MediawikiFactory factory modell
 */
public abstract class LucyFactory {
	
	protected SystemConfig systemConfig;
	
	public abstract LucyDocumentBuilder<?> getDocumentBuilder();
	public abstract LucyIndexBuilder<?> getIndexBuilder();
	public abstract LucyIndexProvider getIndexProvider();
	public abstract LucySearcher getSearcher();
	
	
	public SystemConfig getSystemConfig() {
		return systemConfig;
	}
	
	public LucyFactory(SystemConfig systemConfig){
		this.systemConfig = systemConfig;
	}
	/**
	 * @author Marco Hellenthal tarent GmbH Bonn
	 * 
	 * public static LucyFactory create(SystemConfig conf) instantiates dynamic the factorys
	 * for the given classname in the configfile and creates them
	 */
	@SuppressWarnings("unchecked")
	public static LucyFactory create(SystemConfig conf) {
		try {
			Class<LucyFactory> myFactory = (Class<LucyFactory>) Class.forName(conf.getFactoryClass());
			Constructor<LucyFactory> myFactoryConstructor = myFactory.getDeclaredConstructor(SystemConfig.class);
			return myFactoryConstructor.newInstance(conf);
		} catch (ClassNotFoundException e) {
			LogFactory.log("[lucy-indexer] Could not find the given class : " + conf.getFactoryClass(), e);
		} catch (SecurityException e) {
			LogFactory.log("[lucy-indexer] SecurityException : ", e);
		} catch (NoSuchMethodException e) {
			LogFactory.log("[lucy-indexer] NoSuchMethodException : ", e);
		} catch (IllegalArgumentException e) {
			LogFactory.log("[lucy-indexer] IllegalArgumentException : ", e);
		} catch (InstantiationException e) {
			LogFactory.log("[lucy-indexer] InstantiationException : ", e);
		} catch (IllegalAccessException e) {
			LogFactory.log("[lucy-indexer] IllegalAccessException : ", e);
		} catch (InvocationTargetException e) {
			LogFactory.log("[lucy-indexer] InvocationTargetException : ", e);
		} 
		return null;
	}
}
