package org.evolvis.lucy.indexer.mediawiki;

import java.io.File;
import java.io.IOException;

import java.util.List;
import java.util.Map;

import org.apache.lucene.index.IndexWriter;
import org.evolvis.lucy.config.SystemConfig;
import org.evolvis.lucy.dataaccess.DataFieldNames;
import org.evolvis.lucy.dataaccess.LucyDataAccessException;
import org.evolvis.lucy.dataaccess.MediaWikiDataAccess;
import org.evolvis.lucy.indexer.LucyIndexBuilder;

import de.tarent.commons.logging.LogFactory;


public class MediawikiIndexBuilder extends LucyIndexBuilder<Map<String, String>> {
	
	protected MediawikiDocumentBuilder docBuilder = new MediawikiDocumentBuilder(systemConfig);
	protected int docCount = 1;
	
	public MediawikiIndexBuilder(SystemConfig systemConfig) {
		super(systemConfig);
	}
	/**
	 * @author Marco Hellenthal tarent GmbH Bonn
	 * 
	 * public void createIndex() calls the DataAccess functions iterates over the result
	 * and hands it particular down to the DocumentBuilder
	 */
	@Override
	public void createIndex() throws LucyDataAccessException {
		
		List<Map<String, String>> mediaWikiContent = null;
		List<String> columnNames = new DataFieldNames().getColumnNamesForMediaWikiContent(); 
		
			
				mediaWikiContent = MediaWikiDataAccess.getResultSetForMediaWiki(systemConfig, columnNames);
				File indexPath = new File(systemConfig.getIndexPath());
				
				if(!indexPath.exists())
					indexPath.mkdirs();
				
				for(int i = 0;i < mediaWikiContent.size(); i++){
					indexContent(mediaWikiContent.get(i), indexPath);
				}
			
	}
	/**
	 * @author Marco Hellenthal tarent GmbH Bonn
	 * 
	 * public void indexContent() delivers the data-map to the indexer 
	 * and puts the generated document in the index writer
	 */
	@Override
	protected void indexContent(Map<String, String> content, File indexPath) {
		
		if(content.get("page_namespace").equals("0") || content.get("page_namespace").equals("2") || content.get("page_namespace").equals("4") || content.get("page_namespace").equals("6") || content.get("page_namespace").equals("8") || content.get("page_namespace").equals("10") || content.get("page_namespace").equals("12") || content.get("page_namespace").equals("14") ){
			try {			
				doc = docBuilder.createDocument(content);
				//Delete old document
				checkDoc(indexPath,doc);
				//Write new document
				IndexWriter writer = getWriter(indexPath);
				writer.addDocument( doc );
				docCount++;
				writer.close();
						
			} catch (IOException e) {
				LogFactory.log("[lucy-indexer] Could not find the given index path : " + indexPath.getAbsolutePath(), e);
			}
		}
	}	
	
}
