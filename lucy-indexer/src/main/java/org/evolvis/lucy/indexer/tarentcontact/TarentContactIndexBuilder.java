package org.evolvis.lucy.indexer.tarentcontact;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.lucene.analysis.PerFieldAnalyzerWrapper;
import org.apache.lucene.analysis.de.GermanAnalyzer;
import org.apache.lucene.index.IndexWriter;
import org.evolvis.lucy.config.SystemConfig;
import org.evolvis.lucy.dataaccess.LucyDataAccessException;
import org.evolvis.lucy.dataaccess.TarentContactDataAccess;
import org.evolvis.lucy.indexer.LucyIndexBuilder;

import de.tarent.commons.logging.LogFactory;
import de.tarent.octopus.client.OctopusResult;

public class TarentContactIndexBuilder extends LucyIndexBuilder<OctopusResult> {

	public TarentContactIndexBuilder(SystemConfig systemConfig) {
		super(systemConfig);
	}
	public static ArrayList<String> searchParams;
	
	public void initSearchParams() {
		
		searchParams = new ArrayList<String>();
		searchParams.add("anrede");
		searchParams.add("letteraddress");
		searchParams.add("institutionErweitert");
		searchParams.add("strasse");
		searchParams.add("position");
		searchParams.add("hausNr");
		searchParams.add("fk_user_followup");
		searchParams.add("followup");
		searchParams.add("homepage");
		searchParams.add("fax");
		searchParams.add("vorname");
		searchParams.add("bundesland");
		searchParams.add("kontonummer");
		searchParams.add("spitzname");
		searchParams.add("geschlecht");
		searchParams.add("geburtsjahr");
		searchParams.add("bankleitzahl");
		searchParams.add("captureDate");
		searchParams.add("addressZusatz");
		searchParams.add("ort");
		searchParams.add("followupdate");
		searchParams.add("letteraddress_auto");
		searchParams.add("kurzanrede");
		searchParams.add("faxPrivat");
		searchParams.add("postfachNr");
		searchParams.add("titel");
		searchParams.add("modificationDate");
		searchParams.add("lettersalutation_auto");
		searchParams.add("namenszusatz");
		searchParams.add("geburtsdatum");
		searchParams.add("telefonPrivat");
		searchParams.add("herrnFrau");
		searchParams.add("nameErweitert");
		searchParams.add("bank");
		searchParams.add("postfachNr2");
		searchParams.add("nachname");
		searchParams.add("plz");
		searchParams.add("telefon");
		searchParams.add("telefonMobilPrivat");
		searchParams.add("institution");
		searchParams.add("eMail");
		searchParams.add("abteilung");
		searchParams.add("land");
		searchParams.add("lkz");
		searchParams.add("adrNr");
		searchParams.add("eMailPrivat");
		searchParams.add("akadTitle");
		searchParams.add("telefonMobil");
		
	}
	
	protected TarentContactDocumentBuilder docBuilder = new TarentContactDocumentBuilder(systemConfig);
	
	/**
	 * @author Marco Hellenthal tarent GmbH Bonn
	 * 
	 * public void createIndex() calls the DataAccess functions 
	 * and hands the results down to the Documentbuilder
	 */
	@Override
	public void createIndex() throws LucyDataAccessException {
		TarentContactDataAccess da = new TarentContactDataAccess();
		OctopusResult tcAddressContent = null;
		
		tcAddressContent = da.getAddresses(systemConfig);
		File indexPath = new File(systemConfig.getIndexPath());			
		if(!indexPath.exists())
			indexPath.mkdirs();			
		indexContent(tcAddressContent, indexPath);
		
	}
	/**
	 * @author Marco Hellenthal tarent GmbH Bonn
	 * 
	 * public void indexContent() delivers the data-map to the indexer 
	 * and puts the generated document in the index writer
	 */
	@SuppressWarnings("unchecked")
	@Override
	protected void indexContent(OctopusResult content, File indexPath) {		
		
		List<String> columns = (List<String>) content.getData("columnNames");		
		List<List<Object>> entities = (List<List<Object>>) content.getData("entityTable");
		//Iterate over intances of addresses
		for(List<Object> entity : entities){
			try {				
				doc = docBuilder.createDocument(columns, entity);
				//Delete old document
				checkDoc(indexPath,doc);
			    //Write new document
			    IndexWriter writer = getWriter(indexPath);
			    writer.addDocument( doc );
			    writer.close();
					
			} catch (IOException e) {
				LogFactory.log("[lucy-indexer] Could not find the given index path : " + indexPath.getAbsolutePath(), e);
			}
		}	
	}
	/**
	 * @author Marco Hellenthal tarent GmbH Bonn
	 * 
	 * public PerFieldAnalyzerWrapper getAnalyzer() returns
	 * the required analyzer for the system
	 */
	@Override
	public PerFieldAnalyzerWrapper getAnalyzer() {
		if (analyzer == null) {
			analyzer = new PerFieldAnalyzerWrapper(new GermanAnalyzer());
		}
		initSearchParams();
		for(int i = 0; i < searchParams.size() ; i++){
			//Add german analyzer to each field
			analyzer.addAnalyzer( searchParams.get(i), new GermanAnalyzer() );
		}
		return analyzer;
	}
}
