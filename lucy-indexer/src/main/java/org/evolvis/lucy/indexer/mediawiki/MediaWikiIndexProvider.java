package org.evolvis.lucy.indexer.mediawiki;

import org.evolvis.lucy.config.SystemConfig;
import org.evolvis.lucy.indexer.LucyIndexProvider;

public class MediaWikiIndexProvider extends LucyIndexProvider {

	public MediaWikiIndexProvider(SystemConfig systemConfig) {
		super(systemConfig);
	}

}
