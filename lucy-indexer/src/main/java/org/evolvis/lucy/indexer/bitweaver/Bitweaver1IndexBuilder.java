package org.evolvis.lucy.indexer.bitweaver;

import java.io.File;
import java.util.List;
import java.util.Map;

import org.evolvis.lucy.config.SystemConfig;
import org.evolvis.lucy.dataaccess.BitweaverDataAccess;
import org.evolvis.lucy.dataaccess.DataFieldNames;
import org.evolvis.lucy.dataaccess.LucyDataAccessException;

/**
 * @author Marco Hellenthal tarent GmbH Bonn
 * 
 * The Bitweaver1IndexBuilder, the class to get
 */

public class Bitweaver1IndexBuilder extends BitweaverIndexBuilder {
	
	public Bitweaver1IndexBuilder(SystemConfig systemConfig) {
		super(systemConfig);
	}
	/**
	 * @author Marco Hellenthal tarent GmbH Bonn
	 * 
	 * public void createIndex() calls the DataAccess functions iterates over the result
	 * and hands it particular down to the Documentbuilder
	 */
	@Override
	public void createIndex() throws LucyDataAccessException {
		//Get the fieldnames, important for building identic indexes
		List<String> contentColumnNames = new DataFieldNames().getColumnNamesForBitweaverContent();
		List<String> blogColumnNames = new DataFieldNames().getColumnNamesForBitweaverBlog();
		List<String> blogPostColumnNames = new DataFieldNames().getColumnNamesForBitweaverBlogPost();	
		
		List<Map<String, String>> bitweaverContent;
		//Get the content to be indexed from the database
		bitweaverContent = BitweaverDataAccess.getListForTable(systemConfig, "tiki_content", contentColumnNames);
		List<Map<String, String>> bitweaverBlogs = BitweaverDataAccess.getListForTable(systemConfig, "tiki_blogs", blogColumnNames);
		List<Map<String, String>> bitweaverBlogPosts = BitweaverDataAccess.getListForTableInnerJoin(systemConfig, "tiki_content", "tiki_blog_posts", blogPostColumnNames);
		
		File indexPath = new File(systemConfig.getIndexPath());
		//check if indexpath exists	
		if(!indexPath.exists())
			indexPath.mkdirs();
		//Index wikipages and other contents
		for(int i = 0;i < bitweaverContent.size(); i++){
			if(!bitweaverContent.get(i).get( "content_type_guid" ).equals("bituser") && !bitweaverContent.get(i).get( "content_type_guid" ).equals("bitcomment") && !bitweaverContent.get(i).get("content_type_guid").equals("bitblogpost"))
				indexContent(bitweaverContent.get(i), indexPath);
		}
		//Index blogcontent
		for(int i = 0;i < bitweaverBlogs.size(); i++){
			indexContent(bitweaverBlogs.get(i), indexPath);
		}
		//Index blogposts
		for(int i = 0;i < bitweaverBlogPosts.size(); i++){
			indexContent(bitweaverBlogPosts.get(i), indexPath);
		}
			
	}
	
}
