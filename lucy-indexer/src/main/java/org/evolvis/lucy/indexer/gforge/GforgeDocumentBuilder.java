package org.evolvis.lucy.indexer.gforge;

import java.util.Map;

import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.evolvis.lucy.config.SystemConfig;
import org.evolvis.lucy.indexer.LucyDocumentBuilder;

public class GforgeDocumentBuilder extends LucyDocumentBuilder<Map<String, String>> {

	public GforgeDocumentBuilder(SystemConfig systemConfig) {
		super(systemConfig);
	}
	/**
	 * @author Marco Hellenthal tarent GmbH Bonn
	 * 
	 * protected Document createDocument(Map<String,String> content) adds
	 * the required fields into the document which should be written in the index
	 */
	protected Document createDocument(Map<String, String> content) {
		Document doc = new Document();
		addSystemName(systemConfig.getSystemName(), doc);
		addTitle(content.get("group_name"), doc);
		addHomepage(content.get("homepage"), doc);		
		addContentId(content.get("group_id"), doc);
		addURL(systemConfig.getBasePath(), doc);
		
		return doc;
	}
	/**
	 * @author Marco Hellenthal tarent GmbH Bonn
	 * 
	 * adds a new field into the document
	 */
	protected void addURL(String basePath, Document doc) {
		String path = "";
		path = basePath + "projects/" + doc.get("title");
		
		Field URL = new Field( "url",
				path,
                Field.Store.YES,
                Field.Index.TOKENIZED );
		doc.add( URL );		
	}
	/**
	 * @author Marco Hellenthal tarent GmbH Bonn
	 * 
	 * adds a new field into the document
	 */
	protected void addHomepage(String data, Document doc){
		String home = "";
		if(data != null){
			home = data;
		} else {
			home = "null";
		}
		
		Field homepage = new Field( "homepage",
				translateUmlauts(home),
                Field.Store.YES,
                Field.Index.TOKENIZED );
		doc.add( homepage );
	}
}
