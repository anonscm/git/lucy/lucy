package org.evolvis.lucy.indexer;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.lucene.document.Document;
import org.apache.lucene.search.Hits;
import org.evolvis.lucy.config.SystemConfig;
import org.evolvis.lucy.hits.LucyHit;
import org.evolvis.lucy.hits.LucyHits;

import de.tarent.commons.logging.LogFactory;



public abstract class LucyIndexProvider {
	
	protected SystemConfig systemConfig;
	
	public LucyIndexProvider(SystemConfig systemConfig) {
		this.systemConfig = systemConfig;
	}
	/**
	 * @author Marco Hellenthal tarent GmbH Bonn
	 * 
	 * this function calls calls the search routine and builds LuvcyHits
	 */ 
	public LucyHits executeQuery(String q) {
		
		File anIndexDir = new File(systemConfig.getIndexPath());
		
		LucyHits hits = new LucyHits();
		
		LucySearcher s = new LucySearcher(systemConfig);
		
		List<LucyHit> hitList = new ArrayList<LucyHit>();
		
		Hits h = s.executeQuery(anIndexDir, q);//get results
		
		for (int i = 0; i < h.length(); i++){
			try {
				Document doc = h.doc(i);
				LucyHit hit = new LucyHit();
				Map<String, String> tmp = new LinkedHashMap<String, String>();
				
				tmp.put("content_id", doc.get("content_id"));//put field from doc to param of a LucyHit
				tmp.put("contentType", doc.get("contentType"));
				tmp.put("data", doc.get("data"));
				tmp.put("title", doc.get("title"));
				tmp.put("url", doc.get("url"));
				tmp.put("name", doc.get("systemName"));
				
				hit.setHit(tmp);
				
				hitList.add(hit);//add hit to hitList		
				
			} catch (IOException e) {
				LogFactory.log("[lucy-indexer] Could not create document.", e);
				e.printStackTrace();
			}
		}
		hits.addAll(hitList);
		
		return hits;
	}
	
}
