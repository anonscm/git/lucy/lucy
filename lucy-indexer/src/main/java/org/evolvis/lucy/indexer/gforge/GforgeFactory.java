package org.evolvis.lucy.indexer.gforge;

import org.evolvis.lucy.config.SystemConfig;
import org.evolvis.lucy.indexer.LucyDocumentBuilder;
import org.evolvis.lucy.indexer.LucyFactory;
import org.evolvis.lucy.indexer.LucyIndexBuilder;
import org.evolvis.lucy.indexer.LucyIndexProvider;
import org.evolvis.lucy.indexer.LucySearcher;
/**
 * @author Marco Hellenthal tarent GmbH Bonn
 * 
 * Bitweaver1Factory factory modell
 */
public class GforgeFactory extends LucyFactory {
	
	protected static LucyDocumentBuilder<?> documentBuilder;
	protected static LucyIndexBuilder<?> indexBuilder;
	protected static LucyIndexProvider indexProvider;
	protected static LucySearcher searcher;

	public GforgeFactory(SystemConfig systemConfig) {
		super(systemConfig);
	}

	@Override
	public LucyDocumentBuilder<?> getDocumentBuilder() {
		if (documentBuilder == null) {
			documentBuilder = new GforgeDocumentBuilder(systemConfig);			
		}
		return documentBuilder;
	}

	@Override
	public LucyIndexBuilder<?> getIndexBuilder() {
		if (indexBuilder == null) {
			indexBuilder = new GforgeIndexBuilder(systemConfig);	
		}
		return indexBuilder;
	}

	@Override
	public LucyIndexProvider getIndexProvider() {
		if (indexProvider == null) {
			indexProvider = new GforgeIndexProvider(systemConfig);			
		}
		return indexProvider;
	}

	@Override
	public LucySearcher getSearcher() {
		if (searcher == null) {
			searcher = new GforgeSearcher(systemConfig);			
		}
		return searcher;
	}

}
