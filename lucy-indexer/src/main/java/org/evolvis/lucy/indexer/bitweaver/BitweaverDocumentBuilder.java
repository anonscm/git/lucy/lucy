package org.evolvis.lucy.indexer.bitweaver;

import java.util.Map;

import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.evolvis.lucy.config.SystemConfig;
import org.evolvis.lucy.indexer.LucyDocumentBuilder;

public class BitweaverDocumentBuilder extends LucyDocumentBuilder<Map<String, String>> {


	public BitweaverDocumentBuilder(SystemConfig systemConfig) {
		super(systemConfig);
	}
	/**
	 * @author Marco Hellenthal tarent GmbH Bonn
	 * 
	 * protected Document createDocument(Map<String,String> content) adds
	 * the required fields into the document which should be written in the index
	 */
	protected Document createDocument(Map<String,String> content) {
		
		Document doc = new Document();
		String contentType = null;
		//If map doesn't include a content_type_guid, it is a blog
		if(content.get("content_type_guid") == null){
			contentType = "wiki_blog";
		} else {
			contentType = content.get("content_type_guid");
		}
		
		addTitle(content.get( "title" ), doc);
		addContentType(content.get( "content_type_guid" ), doc);
		addContentId(content.get( "content_id" ), doc);
		addContent(content.get( "data" ), doc);
		addURL(systemConfig.getBasePath(), contentType, doc );
		addSystemName(systemConfig.getSystemName(), doc);
			
		return doc;
	}
	/**
	 * @author Marco Hellenthal tarent GmbH Bonn
	 * 
	 * adds a new field into the document
	 */
	protected void addURL(String basePath, String contentType,Document doc) {
		String path = "";
		if (contentType.equals("bitpage") || contentType.equals("bitbook"))
  	 		path = basePath + "wiki/index.php?content_id=" + doc.get("content_id");
  	 	else if(contentType.equals("fisheyegallery"))
  	 		path = basePath + "fisheye/view.php?gallery_id=" + doc.get("content_id");
  	 	else if(contentType.equals("fisheyeimage"))
  	 		path = basePath + "fisheye/view.php?image_id=" + doc.get("content_id");
  	 	else if(contentType.equals("pigeonholes"))
  	 		path = basePath + "pigeonholes/view.php?content_id=" + doc.get("content_id");
  	 	else if(contentType.equals("bitblogpost"))
  	 		path = basePath + "blogs/view_post.php?content_id=" + doc.get("content_id");
  	 	else {
  	 		path = contentType;
  	 	}
		Field URL = new Field( "url",
				path,
                Field.Store.YES,
                Field.Index.TOKENIZED );
		doc.add( URL );
		
	}
	/**
	 * @author Marco Hellenthal tarent GmbH Bonn
	 * 
	 * adds a new field into the document
	 */
	protected void addContent(String data, Document doc){
		String dat = "";
		if(data != null){
			dat = data;
		} else {
			dat = "null";
		}
		Field dataField = new Field( "data",
				translateUmlauts(dat),
                Field.Store.YES,
                Field.Index.TOKENIZED );
		dataField.setBoost(BOOST_DATA);
		doc.add( dataField );		
	}
	/**
	 * @author Marco Hellenthal tarent GmbH Bonn
	 * 
	 * adds a new field into the document
	 */
	protected void addContentType(String data, Document doc){
		String contentType = "";
		if(data != null){
			contentType = data;
		} else {
			contentType = "null";
		}
		Field conType = new Field( "contentType",
				contentType,
                Field.Store.YES,
                Field.Index.UN_TOKENIZED);
		doc.add( conType );				
	}

}
