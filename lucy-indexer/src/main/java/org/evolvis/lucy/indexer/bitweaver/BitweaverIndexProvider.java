package org.evolvis.lucy.indexer.bitweaver;

import org.evolvis.lucy.config.SystemConfig;
import org.evolvis.lucy.indexer.LucyIndexProvider;


public class BitweaverIndexProvider extends LucyIndexProvider {

	public BitweaverIndexProvider(SystemConfig systemConfig) {
		super(systemConfig);
	}
}
