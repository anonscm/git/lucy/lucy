package org.evolvis.lucy.indexer.tarentcontact;

import org.evolvis.lucy.config.SystemConfig;
import org.evolvis.lucy.indexer.LucyDocumentBuilder;
import org.evolvis.lucy.indexer.LucyFactory;
import org.evolvis.lucy.indexer.LucyIndexBuilder;
import org.evolvis.lucy.indexer.LucyIndexProvider;
import org.evolvis.lucy.indexer.LucySearcher;


public class TarentContactFactory extends LucyFactory {
	
	protected static LucyDocumentBuilder<?> documentBuilder;
	protected static LucyIndexBuilder<?> indexBuilder;
	protected static LucyIndexProvider indexProvider;
	protected static LucySearcher searcher;	

	public TarentContactFactory(SystemConfig systemConfig) {
		super(systemConfig);
	}

	@Override
	public LucyDocumentBuilder<?> getDocumentBuilder() {
		if (documentBuilder == null) {
			documentBuilder = new TarentContactDocumentBuilder(systemConfig);			
		}
		return documentBuilder;
	}

	@Override
	public LucyIndexBuilder<?> getIndexBuilder() {
		if (indexBuilder == null) {
			indexBuilder = new TarentContactIndexBuilder(systemConfig);			
		}
		return indexBuilder;
	}

	@Override
	public LucyIndexProvider getIndexProvider() {
		if (indexProvider == null) {
			indexProvider = new TarentContactIndexProvider(systemConfig);			
		}
		return indexProvider;
	}

	@Override
	public LucySearcher getSearcher() {
		if (searcher == null) {
			searcher = new TarentContactSearcher(systemConfig);			
		}
		return searcher;
	}

}
