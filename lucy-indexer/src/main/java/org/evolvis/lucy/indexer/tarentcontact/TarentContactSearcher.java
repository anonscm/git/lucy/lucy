package org.evolvis.lucy.indexer.tarentcontact;

import java.io.File;
import java.io.IOException;

import org.apache.lucene.queryParser.MultiFieldQueryParser;
import org.apache.lucene.queryParser.ParseException;
import org.apache.lucene.queryParser.QueryParser;
import org.apache.lucene.search.Hits;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.evolvis.lucy.config.SystemConfig;
import org.evolvis.lucy.indexer.*;

public class TarentContactSearcher extends LucySearcher {

	public TarentContactSearcher(SystemConfig systemConfig) {
		super(systemConfig);
	}
	/**
	 * @author Marco Hellenthal tarent GmbH Bonn
	 * 
	 * Method for browsing through the index with addresses
	 */
	@Override
	public Hits executeQuery(File indexDir, String q) {
		Directory fsDir;
		IndexSearcher is;
		Hits hits = null;
		try {
			fsDir = FSDirectory.getDirectory(indexDir);//directory where index can be found
			is = new IndexSearcher(fsDir);
			//Build parser
			QueryParser qp = new MultiFieldQueryParser(new String[]{"anrede", "letteraddress", "institutionErweitert", "strasse", "position", "hausNr", "fk_user_followup",
			                                   		              "followup", "homepage", "fax", "vorname", "bundesland", "kontonummer", "spitzname", "geschlecht", "geburtsjahr",
			                                		             "bankleitzahl", "captureDate", "addressZusatz", "ort", "followupdate", "letteraddress_auto", "kurzanrede",
			                                		              "faxPrivat", "postfachNr", "titel", "modificationDate", "lettersalutation_auto", "namenszusatz", "geburtsdatum",
			                                		              "telefonPrivat", "herrnFrau", "nameErweitert", "bank", "postfachNr2", "nachname", "plz", "telefon", "telefonMobilPrivat",
			                                		             "institution", "eMail", "abteilung", "land", "lkz", "adrNr", "eMailPrivat", "akadTitle", "telefonMobil"}, LucyRegistry.getInstance().getSystemByName(systemConfig.getSystemName()).getIndexBuilder().getAnalyzer() );
			qp.setDefaultOperator(QueryParser.AND_OPERATOR);
			Query query;
			query = qp.parse(q);
			hits = is.search(query);
		} catch (IOException e) {
			System.out.println("[lucy-indexer] Could not find the given index directory: " + indexDir.getAbsolutePath());
			e.printStackTrace();
		} catch (ParseException e) {
			System.out.println("[lucy-indexer] Could not create query for: " + q);
			e.printStackTrace();
		}
		return hits;
	}

}
