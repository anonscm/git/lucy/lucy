package org.evolvis.lucy.indexer.gforge;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Map;

import org.apache.lucene.index.IndexWriter;
import org.evolvis.lucy.config.SystemConfig;
import org.evolvis.lucy.dataaccess.DataFieldNames;
import org.evolvis.lucy.dataaccess.GforgeDataAccess;
import org.evolvis.lucy.dataaccess.LucyDataAccessException;
import org.evolvis.lucy.indexer.LucyIndexBuilder;

import de.tarent.commons.logging.LogFactory;

public class GforgeIndexBuilder extends LucyIndexBuilder<Map<String, String>> {
	
	protected GforgeDocumentBuilder docBuilder = new GforgeDocumentBuilder(systemConfig);
	
	public GforgeIndexBuilder(SystemConfig systemConfig) {
		super(systemConfig);
	}
	/**
	 * @author Marco Hellenthal tarent GmbH Bonn
	 * 
	 * public void createIndex() calls the DataAccess functions iterates over the result
	 * and hands it particular down to the Documentbuilder
	 */
	@Override
	public void createIndex() throws LucyDataAccessException {
		DataFieldNames fields = new DataFieldNames();
		List<String> columnNames = fields.getColumnNamesForGForgeContent();
		List<Map<String, String>> content;
		//Get data from database
		content = GforgeDataAccess.getResultSetForGforge(systemConfig, columnNames);
		File indexPath = new File(systemConfig.getIndexPath());
		
		if(!indexPath.exists())
			indexPath.mkdirs();
		//Iteration over columns of documents
		for(int i = 0;i < content.size(); i++){
			indexContent(content.get(i), indexPath);
		}					
	}
	/**
	 * @author Marco Hellenthal tarent GmbH Bonn
	 * 
	 * public void indexContent() delivers the data-map to the indexer 
	 * and puts the generated document in the index writer
	 */
	@Override
	protected void indexContent(Map<String, String> content, File indexPath) {
		try {		
			doc = docBuilder.createDocument(content);
			//Delete old document
			checkDoc(indexPath,doc);
		    //Write new document
		    IndexWriter writer = getWriter(indexPath);
		    writer.addDocument( doc );
		    writer.close();
		} catch (IOException e) {
			LogFactory.log("[lucy-indexer] Could not find the given index path : " + indexPath.getAbsolutePath(), e);
		}
		
	}

}
