package org.evolvis.lucy.indexer;

import java.io.File;
import java.io.IOException;

import org.apache.lucene.queryParser.MultiFieldQueryParser;
import org.apache.lucene.queryParser.ParseException;
import org.apache.lucene.queryParser.QueryParser;
import org.apache.lucene.search.Hits;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.evolvis.lucy.config.SystemConfig;
import org.evolvis.lucy.indexer.LucyRegistry;

import de.tarent.commons.logging.LogFactory;

	public class LucySearcher {
		
		protected SystemConfig systemConfig;
		public LucySearcher( SystemConfig systemConfig) {
			this.systemConfig = systemConfig;
		}
		/**
		 * @author Marco Hellenthal tarent GmbH Bonn
		 * 
		 * Method for browsing through the index with titel and data
		 */ 
		public Hits executeQuery(File indexDir, String q)  {
			Directory fsDir;
			IndexSearcher is;
			Hits hits = null;
			try {
				fsDir = FSDirectory.getDirectory(indexDir);//Directory where index can be found		
				is = new IndexSearcher(fsDir);//Build searcher for the given directory
				QueryParser qp = new MultiFieldQueryParser(new String[]{"title", "data"}, LucyRegistry.getInstance().getSystemByName(systemConfig.getSystemName()).getIndexBuilder().getAnalyzer() );
				//Queryparser rolls over the given fields
				qp.setDefaultOperator(QueryParser.AND_OPERATOR);//
				Query query;
				query = qp.parse(q);//search in the fields we gave the parser
				hits = is.search(query);
			} catch (IOException e) {
				LogFactory.log("[lucy-indexer] Could not find the given index directory: " + indexDir.getAbsolutePath(), e);
				e.printStackTrace();
			} catch (ParseException e) {
				LogFactory.log("[lucy-indexer] Could not create query for: " + q, e);
				e.printStackTrace();
			}
			return hits;
		}
	}



