package org.evolvis.lucy.indexer.bitweaver;

import java.io.File;
import java.util.List;
import java.util.Map;

import org.evolvis.lucy.config.SystemConfig;
import org.evolvis.lucy.dataaccess.BitweaverDataAccess;
import org.evolvis.lucy.dataaccess.DataFieldNames;
import org.evolvis.lucy.dataaccess.LucyDataAccessException;

public class Bitweaver2IndexBuilder extends BitweaverIndexBuilder {

	public Bitweaver2IndexBuilder(SystemConfig systemConfig ) {
		super(systemConfig);
	}
	/**
	 * @author Marco Hellenthal tarent GmbH Bonn
	 * 
	 * public void createIndex() calls the DataAccess functions iterates over the result
	 * and hands it particular down to the Documentbuilder
	 */
	@Override
	public void createIndex() throws LucyDataAccessException {
		//Get the fieldnames, important for building identic indexes
		List<String> contentColumnNames = new DataFieldNames().getColumnNamesForBitweaverContent();
		List<String> blogColumnNames = new DataFieldNames().getColumnNamesForBitweaverBlog();
		List<String> blogPostColumnNames = new DataFieldNames().getColumnNamesForBitweaverBlogPost();
		
		
		List<Map<String, String>> bitweaverContent;
		//Get the content to be indexed from the database
		bitweaverContent = BitweaverDataAccess.getListForTable(systemConfig, "liberty_content", contentColumnNames);
		List<Map<String, String>> bitweaverBlogs = BitweaverDataAccess.getListForTableInnerJoin(systemConfig, "liberty_content", "blogs", blogColumnNames);
		List<Map<String, String>> bitweaverBlogPosts = BitweaverDataAccess.getListForTableInnerJoin(systemConfig,"liberty_content", "blog_posts", blogPostColumnNames);
		
		File indexPath = new File(systemConfig.getIndexPath());
			
		if(!indexPath.exists())
			indexPath.mkdirs();
		
		//Index wikipages and other contents	
		for(int i = 0; i < bitweaverContent.size(); i++){
			indexContent(bitweaverContent.get(i), indexPath);
		}
		//Index blogcontent
		for(int i = 0; i < bitweaverBlogs.size(); i++){
			indexContent(bitweaverBlogs.get(i), indexPath);
		}
		//Index blogposts
		for(int i = 0; i < bitweaverBlogPosts.size(); i++){
			indexContent(bitweaverBlogPosts.get(i), indexPath);
		}		
	}	
}
