package org.evolvis.lucy.indexer.tarentcontact;

import java.util.List;

import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.evolvis.lucy.config.SystemConfig;
import org.evolvis.lucy.indexer.LucyDocumentBuilder;

import de.tarent.octopus.client.OctopusResult;

public class TarentContactDocumentBuilder extends LucyDocumentBuilder<OctopusResult> {

	public TarentContactDocumentBuilder(SystemConfig systemConfig) {
		super(systemConfig);
	}
	/**
	 * @author Marco Hellenthal tarent GmbH Bonn
	 * 
	 * This function for this system is responsable for building a tarent contact document.
	 * In this case we are not really talkig about documents whith text, but
	 * about a document including multi field adresses.
	 */
	protected Document createDocument(List<String> columns, List<Object> address) {
		
		int i = 0;
		Document doc = new Document();
		//Add seperate fields of the addresses to the document
		for(Object value : address ) {
			
			String val = "";				
			if(value != null){
				val = value.toString();
			} else {
				val = "n/a";
			}				
			Field tmp = new Field( columns.get(i),
			translateUmlauts(val),
			Field.Store.YES,
			Field.Index.TOKENIZED );
			doc.add( tmp );
			i++;
			
		}
		addContentId(doc.get("adrNr"), doc);
		addSystemName(systemConfig.getSystemName(), doc);
			
		return doc;

	}

}
