package org.evolvis.lucy.indexer.bitweaver;

import org.evolvis.lucy.config.SystemConfig;
import org.evolvis.lucy.indexer.LucyDocumentBuilder;
import org.evolvis.lucy.indexer.LucyFactory;
import org.evolvis.lucy.indexer.LucyIndexBuilder;
import org.evolvis.lucy.indexer.LucyIndexProvider;
import org.evolvis.lucy.indexer.LucySearcher;

/**
 * @author Marco Hellenthal tarent GmbH Bonn
 * 
 * Bitweaver1Factory factory modell
 */
public class Bitweaver1Factory extends LucyFactory {
	//Components of the factory
	protected static LucyDocumentBuilder<?> documentBuilder;
	protected static LucyIndexBuilder<?> indexBuilder;
	protected static LucyIndexProvider indexProvider;
	protected static LucySearcher searcher;

	
	public Bitweaver1Factory(SystemConfig systemConfig) {
		super(systemConfig);
	}

	@Override
	public LucyDocumentBuilder<?> getDocumentBuilder() {
		if(documentBuilder == null) {
			documentBuilder = new BitweaverDocumentBuilder(systemConfig);
		}
		return documentBuilder;
	}

	@Override
	public LucyIndexBuilder<?> getIndexBuilder() {
		if(indexBuilder == null) {
			indexBuilder = new Bitweaver1IndexBuilder(systemConfig);
		}
		return indexBuilder;
	}

	@Override
	public LucyIndexProvider getIndexProvider() {
		if(indexProvider == null) {
			indexProvider = new BitweaverIndexProvider(systemConfig);
		}
		return indexProvider;
	}

	@Override
	public LucySearcher getSearcher() {
		if(searcher == null) {
			searcher = new LucySearcher(systemConfig);
		}
		return searcher;
	}
}
