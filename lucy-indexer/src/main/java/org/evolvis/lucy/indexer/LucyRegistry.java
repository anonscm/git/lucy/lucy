package org.evolvis.lucy.indexer;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Marco Hellenthal tarent GmbH Bonn
 * 
 * This is a class which is reachable in runtime, for getting information
 * about the systems which are already indexed and searchable. it is
 * implemented as a singleton
 */

public class LucyRegistry {
	
	private static LucyRegistry registry = null;
	
	protected Map<String, LucyFactory> systems = new LinkedHashMap<String, LucyFactory>();
	
	public static LucyRegistry getInstance() {
		if (registry == null){
			registry = new LucyRegistry();
		}
		return registry;
	}
		
	public void put(String systemName, LucyFactory system){
		systems.put(systemName, system);	
	}
	
	public void remove(String systemName){
		systems.remove(systemName);
	}
	
	public LucyFactory getSystemByName(String systemName){
		return systems.get(systemName);
	}

	public Map<String, LucyFactory> getSystems() {
		return systems;
	}

	public  void setSystems(Map<String, LucyFactory> systems) {
		this.systems = systems;
	}
	
	public List<String> getNames() {
		List<String> list = new ArrayList<String>();
		list.addAll(systems.keySet());
		return list;
	}
}
