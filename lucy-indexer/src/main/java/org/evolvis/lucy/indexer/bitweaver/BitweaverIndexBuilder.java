package org.evolvis.lucy.indexer.bitweaver;

import java.io.File;
import java.io.IOException;
import java.util.Map;

import org.apache.lucene.index.IndexWriter;
import org.evolvis.lucy.config.SystemConfig;
import org.evolvis.lucy.indexer.LucyIndexBuilder;

import de.tarent.commons.logging.LogFactory;

public abstract class BitweaverIndexBuilder extends LucyIndexBuilder<Map<String, String>> {
	
	public BitweaverIndexBuilder(SystemConfig systemConfig) {
		super(systemConfig);
	}
	protected BitweaverDocumentBuilder docBuilder = new BitweaverDocumentBuilder(systemConfig);
	
	/**
	 * @author Marco Hellenthal tarent GmbH Bonn
	 * 
	 * public void indexContent() delivers the data-map to the indexer 
	 * and puts the generated document in the index writer
	 */
	@Override
	protected void indexContent(Map<String, String> content, File indexPath) {	
	
		try {			
			doc = docBuilder.createDocument(content);
			//Delete old document
			checkDoc(indexPath,doc);
			IndexWriter writer = getWriter(indexPath);//Get writer, for writing the index into the filesystem
		    writer.addDocument( doc );//Add document to the writer which should be included to the index
		    writer.close();//Close the writer and add document to the index
		} catch (IOException e) {
			LogFactory.log("[lucy-indexer] Could not find the given index path : " + indexPath.getAbsolutePath(), e);
		}
		
	}
}
