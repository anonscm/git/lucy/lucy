package org.evolvis.lucy.config;

import java.util.List;
import javax.xml.bind.annotation.XmlElement;

/**
 * @author Marco Hellenthal tarent GmbH Bonn
 * 
 * The IndexerSystemConfig describes the xml structure
 * of our lucy-configuration in java
 */

public class IndexerSystemConfig {
	
	protected List<SystemConfig> systems;
	
	@XmlElement(name = "system")//declare name for node in xml file
	public List<SystemConfig> getSystems() {
		return systems;
	}

	public void setSystems(List<SystemConfig> systems) {
		this.systems = systems;
	}

}
