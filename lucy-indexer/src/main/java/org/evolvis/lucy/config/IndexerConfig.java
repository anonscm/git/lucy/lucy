package org.evolvis.lucy.config;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;


/**
 * @author Marco Hellenthal tarent GmbH Bonn
 * 
 * The IndexerConfig describes the xml structure
 * of our lucy-configuration in java.
 */

@XmlRootElement(name="indexerConfig")
public class IndexerConfig {
	
	protected IndexerSystemConfig systemFactory;
	
	@XmlElement(name = "systems")//declare name for node in xml file
	public IndexerSystemConfig getSystemFactory() {
		return systemFactory;
	}

	public void setSystemFactory(IndexerSystemConfig systemFactory) {
		this.systemFactory = systemFactory;
	}
}
