package org.evolvis.lucy.config;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import javax.xml.bind.JAXBException;
import javax.xml.stream.FactoryConfigurationError;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

import org.evolvis.scio.bind.BindContext;
import org.evolvis.scio.bind.Marshaller;
import org.evolvis.scio.bind.Unmarshaller;

import de.tarent.commons.logging.LogFactory;

/**
 * @author Marco Hellenthal tarent GmbH Bonn
 * 
 * The XmlConfigParser offers functions to read and write xml from file
 */

public class XmlConfigParser {
	
	/**
	 * @author Marco Hellenthal tarent GmbH Bonn
	 * 
	 * public IndexerConfig deserialize(InputStream in) reads the configuration from the xml file
	 */
	public IndexerConfig deserialize(InputStream in) {
		IndexerConfig configuration = null;
		
		XMLStreamReader reader = null;
		try {
			reader = XMLInputFactory.newInstance().createXMLStreamReader(in);//get reader to file
		} catch (XMLStreamException e) {
			LogFactory.log("[Lucy-Indexer] Could not create StreamReader for xml configuration.", e);
		} catch (FactoryConfigurationError e1) {
			LogFactory.log("[Factory Configuration does not work." + e1.toString(), null);
		}
		Unmarshaller unmarshaller = null;
		try {
			unmarshaller = getBindContext("org.evolvis.lucy.config").createUnarshaller();//get Unmarshaller
			configuration = (IndexerConfig) unmarshaller.unmarshal(reader);//cast config from file to IndexerConfig
			reader.close();
		} catch (JAXBException e) {
			LogFactory.log("Could not find the given BindContext." , e);
		} catch (XMLStreamException e) {
			LogFactory.log("Could free the resources associated with the reader." , e);
		}
		return configuration;
	}
	/**
	 * @author Marco Hellenthal tarent GmbH Bonn
	 * 
	 * public void serialize(IndexerConfig configuration, OutputStream out) writes the configuration into the xml file
	 */
	public void serialize(IndexerConfig configuration, OutputStream out){
		Marshaller marshaller = null;
		try {
			marshaller = getBindContext("org.evolvis.lucy.config").createMarshaller();//create a marshaller
			marshaller.marshal(configuration, out);//write configuration to file in the OutputStream
			out.close();
		} catch (JAXBException e) {
			LogFactory.log("Could not find the given BindContext." , e);
		} catch (IOException e) {
			LogFactory.log("Could not find the xml configuration file" , e);
		}
	}
	/**
	 * @author Marco Hellenthal tarent GmbH Bonn
	 * 
	 * public BindContext getBindContext(String packageName) reads the jaxb.index and instantiates the needed classes
	 */
	public BindContext getBindContext(String packageName) throws JAXBException{
		BindContext cntx = null;
		try {
			cntx = BindContext.newInstance(packageName);//get the bindcontext from the given packagename
		} catch (JAXBException e) {
			LogFactory.log("Could not find the given BindContext." , e);
		}
		return cntx;
	}
}
