package org.evolvis.lucy.config;

import java.util.LinkedHashMap;
import java.util.Map;

import javax.xml.bind.annotation.XmlAttribute;

/**
 * @author Marco Hellenthal tarent GmbH Bonn
 * 
 * The SystemConfig contains all possible attributes
 * for configuring our in lucy integrated systems
 */

public class SystemConfig {	
	
	protected String basePath;
	protected String factoryClass;
	protected String systemName;
	protected String indexPath;
	protected String poolName;	
	protected String type;
	protected String module;
	protected String authType;
	protected String autoLogin;
	protected String username;
	protected String password;
	protected String keepSessionAlive;
	protected String serviceURL;
	protected String connectionTracking;
	protected String useSessionCookie;
	protected String sessionCookieFile;
	
	protected static Map<String, String> config = new LinkedHashMap<String, String>();
	
	
	@XmlAttribute(name="factoryClass")//declare name for attribute in xml file
	public String getFactoryClass() {
		return factoryClass;
	}
	
	public void setFactoryClass(String factoryClass) {
		this.factoryClass = factoryClass;
	}
	
	public String getBasePath() {
		return basePath;
	}
	
	public void setBasePath(String basePath) {
		this.basePath = basePath;
	}

	public String getIndexPath() {
		return indexPath;
	}

	public void setIndexPath(String indexPath) {
		this.indexPath = indexPath;
	}

	public String getSystemName() {
		return systemName;
	}

	public void setSystemName(String systemName) {
		this.systemName = systemName;
	}

	public String getPoolName() {
		return poolName;
	}

	public void setPoolName(String poolName) {
		this.poolName = poolName;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getModule() {
		return module;
	}

	public void setModule(String module) {
		this.module = module;
	}

	public String getAuthType() {
		return authType;
	}

	public void setAuthType(String authType) {
		this.authType = authType;
	}

	public String getAutoLogin() {
		return autoLogin;
	}

	public void setAutoLogin(String autoLogin) {
		this.autoLogin = autoLogin;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getKeepSessionAlive() {
		return keepSessionAlive;
	}

	public void setKeepSessionAlive(String keepSessionAlive) {
		this.keepSessionAlive = keepSessionAlive;
	}

	public String getServiceURL() {
		return serviceURL;
	}

	public void setServiceURL(String serviceURL) {
		this.serviceURL = serviceURL;
	}

	public String getConnectionTracking() {
		return connectionTracking;
	}

	public void setConnectionTracking(String connectionTracking) {
		this.connectionTracking = connectionTracking;
	}

	public String getUseSessionCookie() {
		return useSessionCookie;
	}

	public void setUseSessionCookie(String useSessionCookie) {
		this.useSessionCookie = useSessionCookie;
	}

	public String getSessionCookieFile() {
		return sessionCookieFile;
	}

	public void setSessionCookieFile(String sessionCookieFile) {
		this.sessionCookieFile = sessionCookieFile;
	}
	
	public Map<String, String> getOctopusRemoteConfigAsMap(){
		initMap();
		return config;
	}
	protected void initMap(){
		config.put("type", type);
		config.put("module", module);
		config.put("authType", authType);
		config.put("autoLogin", autoLogin);
		config.put("username", username);
		config.put("password", password);
		config.put("keepSessionAlive", keepSessionAlive);
		config.put("serviceURL", serviceURL);
		config.put("connectionTracking", connectionTracking);
		config.put("useSessionCookie", useSessionCookie);
		config.put("sessionCookieFile", sessionCookieFile);
		
	}
	protected void put(String key, String value){
		config.put(key, value);		
	}
	
}
