package org.evolvis.lucy.hits;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Marco Hellenthal tarent GmbH Bonn
 * 
 * bean for searchresults
 */

public class LucyHits extends ArrayList<LucyHit> {

	private static final long serialVersionUID = 1L;

	public List<LucyHit> getHits() {
		return this;
	}

	public void setHits(LucyHits hits) {
		this.addAll(hits);
	}

}
