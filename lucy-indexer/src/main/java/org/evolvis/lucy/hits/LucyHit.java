package org.evolvis.lucy.hits;

import java.util.Map;

/**
 * @author Marco Hellenthal tarent GmbH Bonn
 * 
 * bean for searchresults
 */

public class LucyHit {
	
	Map<String, String> hit;

	public Map<String, String> getHit() {
		return hit;
	}

	public void setHit(Map<String, String> hit) {
		this.hit = hit;
	}		

}
