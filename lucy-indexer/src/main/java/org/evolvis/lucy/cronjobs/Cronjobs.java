package org.evolvis.lucy.cronjobs;

import java.util.List;

import org.evolvis.lucy.config.SystemConfig;
import org.evolvis.lucy.dataaccess.LucyDataAccessException;
import org.evolvis.lucy.indexer.LucyFactory;
import org.evolvis.lucy.indexer.LucyRegistry;

import de.tarent.commons.logging.LogFactory;
/**
 * @author Marco Hellenthal tarent GmbH Bonn
 * 
 * The only Cronjob in lucy is a algorithm for
 * rebuilding the indexes of all systems in the registry
 */
public class Cronjobs extends Thread {
	
	public Cronjobs() {

	}
	/**
	 * @author Marco Hellenthal tarent GmbH Bonn
	 * 
	 * public void run() is a Cronjobroutine for rebuilding the lucy index
	 */
	@Override
	public void run() {
		List<String> list = LucyRegistry.getInstance().getNames(); //get the names of the indexed systems from the registry
		for(int i = 0;i<list.size();i++){
			LucyFactory system = LucyRegistry.getInstance().getSystemByName(list.get(i));//get system in i position
			SystemConfig systemConf = system.getSystemConfig();//get the config
			LogFactory.log("[lucy-indexer] Rebuilding " + systemConf.getSystemName(), null);
			try {
				system.getIndexBuilder().rebuild(); //reduild the systems index
			} catch (LucyDataAccessException e) {
				e.printStackTrace();
			}
			LogFactory.log("[lucy-indexer] Rebuild for " + systemConf.getSystemName() + " successfully finished!", null);
		}
		super.run();
	}
}
