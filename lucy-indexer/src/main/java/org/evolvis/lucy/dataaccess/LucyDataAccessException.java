package org.evolvis.lucy.dataaccess;

/**
 * @author Marco Hellenthal tarent GmbH Bonn
 * 
 * This exception is thrown when a system isn't reachable
 */

public class LucyDataAccessException extends Exception {
	
	private static final long serialVersionUID = 1L;
	
	public LucyDataAccessException() {
		super();
	}
	
	public LucyDataAccessException(String e, Throwable cause) {
		super("Couldn't reach system : " + e + " please review the config.");
	}

}
