package org.evolvis.lucy.dataaccess;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.evolvis.lucy.config.SystemConfig;

import de.tarent.dblayer.engine.DB;
import de.tarent.dblayer.engine.DBContext;
import de.tarent.dblayer.engine.ResultProcessor;
import de.tarent.dblayer.sql.SQL;
import de.tarent.dblayer.sql.statement.Select;

/**
 * @author Marco Hellenthal tarent GmbH Bonn
 * 
 * GforgeDataAccess fetches the required data for building
 * the index of an Gforge-system
 */

public class GforgeDataAccess {

public static List<Map<String, String>> getResultSetForGforge(SystemConfig systemConfig,final List<String> columnNames) throws LucyDataAccessException {
	
		final List<Map<String, String>> rows = new ArrayList<Map<String, String>>();
		
		DBContext dbc = DB.getDefaultContext(systemConfig.getPoolName());
		try {
			if (dbc == null || dbc.getDefaultConnection() == null)
				return null;
		} catch (SQLException e1) {
			throw new LucyDataAccessException(systemConfig.getSystemName(), e1);
		}
		
		Select s = SQL.Select(dbc);
		s.from("groups");
		s.select("short_description");
		s.select("group_name");
		s.select("homepage");
		s.select("group_id");		

		final int numWantedColumns = columnNames.size();
		
				try {
						s.iterate(new ResultProcessor() {
							public void process(ResultSet rs) throws SQLException {					    
						     
						            Map<String, String> row = new LinkedHashMap<String, String>();
						 
						            for (int i = 0; i < numWantedColumns; ++i)
						            {
						                String columnName   = (String)columnNames.get(i);
						                String value = rs.getString(columnName);
						                row.put(columnName, value);
						            }			 
						            rows.add(row);					
							}				
						});
						dbc.getDefaultConnection().close();
						return rows;
				} catch (SQLException e) {
					throw new LucyDataAccessException(systemConfig.getSystemName(), e);
				}				
}}
