package org.evolvis.lucy.dataaccess;

import java.util.Map;

import org.evolvis.lucy.config.SystemConfig;


import de.tarent.octopus.client.OctopusCallException;
import de.tarent.octopus.client.OctopusConnection;
import de.tarent.octopus.client.OctopusConnectionFactory;
import de.tarent.octopus.client.OctopusResult;


/**
 * @author Marco Hellenthal tarent GmbH Bonn
 * 
 * Webservice call for getting the addresses from the given tarent contact system
 */

public class TarentContactDataAccess {
	
	public OctopusResult getAddresses(SystemConfig conf) throws LucyDataAccessException {
		OctopusResult res = null;
		try {		
			Map<String, String> orc = conf.getOctopusRemoteConfigAsMap();
			
			OctopusConnectionFactory.getInstance().setConfiguration("config", orc);
			OctopusConnection con = OctopusConnectionFactory.getInstance().getConnection("config");//get connection
			res = con.getTask("getAddresses").invoke();
		} catch (OctopusCallException e) {
			throw new LucyDataAccessException(conf.getSystemName(), e);
		}
		return res;
	}
}