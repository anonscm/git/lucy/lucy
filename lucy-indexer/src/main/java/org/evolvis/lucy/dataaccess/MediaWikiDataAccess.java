package org.evolvis.lucy.dataaccess;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.evolvis.lucy.config.SystemConfig;

import de.tarent.commons.logging.LogFactory;
import de.tarent.dblayer.engine.DB;
import de.tarent.dblayer.engine.DBContext;
import de.tarent.dblayer.engine.ResultProcessor;
import de.tarent.dblayer.sql.SQL;
import de.tarent.dblayer.sql.statement.Select;

/**
 * @author Marco Hellenthal tarent GmbH Bonn
 * 
 * MediaWikiDataAccess fetches the required data for building
 * the index of an MediaWiki-system
 */

public class MediaWikiDataAccess {
	
	public static List<Map<String, String>> getResultSetForMediaWiki(SystemConfig systemConfig,final List<String> columnNames) throws LucyDataAccessException {
		
		final List<Map<String, String>> rows = new ArrayList<Map<String, String>>();
		
		try{
			DBContext dbc = DB.getDefaultContext(systemConfig.getPoolName());			
			
			if (dbc == null)
				return null;
			
			Select s = SQL.Select(dbc);
			s.from("page");
			s.select("page.page_title");
			s.select("page.page_namespace");
			s.select("searchindex.si_text");
			s.select("searchindex.si_page");
			s.join("searchindex", "page.page_id", "searchindex.si_page");		
	
			final int numWantedColumns = columnNames.size();
			
				s.iterate(new ResultProcessor() {
					public void process(ResultSet rs) throws SQLException {					    
				     
						Map<String, String> row = new LinkedHashMap<String, String>();
				 
				        for (int i = 0; i < numWantedColumns; ++i)
				        {
				        	String columnName   = (String)columnNames.get(i);
				            String value = rs.getString(columnName);
				            row.put(columnName, value);
				        }			 
				        rows.add(row);					
					}				
				});			
			dbc.getDefaultConnection().close();
		} catch (SQLException e) {
			LogFactory.log("[lucy-indexer] Could not create List from ResultSet.", e);
			throw new LucyDataAccessException(systemConfig.getSystemName(), e);	
		}
			
		return rows;					
			
	}

}
