package org.evolvis.lucy.dataaccess;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.evolvis.lucy.config.SystemConfig;

import de.tarent.dblayer.engine.DB;
import de.tarent.dblayer.engine.DBContext;
import de.tarent.dblayer.engine.ResultProcessor;
import de.tarent.dblayer.sql.SQL;
import de.tarent.dblayer.sql.statement.Select;

/**
 * @author Marco Hellenthal tarent GmbH Bonn
 * 
 * BitweaverDataAccess fetches the required data for building
 * the index of an Bitweaver-system
 */

public class BitweaverDataAccess {

		
		public static List<Map<String, String>> getListForTable(SystemConfig systemConfig, String table_name,final List<String> columnNames) throws LucyDataAccessException {
		
		final List<Map<String, String>> rows = new ArrayList<Map<String, String>>();
	
			DBContext dbc = DB.getDefaultContext(systemConfig.getPoolName());
			
			if (dbc == null)
				return null;
			
			Select s = SQL.Select(dbc);
			s.from(table_name);//set table name
			for(int i = 0; i < columnNames.size(); i++){//iterate fieldnames
				s.select(table_name + "." + columnNames.get(i));
			}

			final int numWantedColumns = columnNames.size();
			try {
				s.iterate(new ResultProcessor() {//wrap results to List<Map<String, String>>
					public void process(ResultSet rs) throws SQLException {					    
				     
				            Map<String, String> row = new LinkedHashMap<String, String>();
				 
				            for (int i = 0; i < numWantedColumns; ++i)
				            {
				                String columnName   = (String)columnNames.get(i);
				                String value = rs.getString(columnName);
				                row.put(columnName, value);
				            }			 
				            rows.add(row);					
					}				
				});
			dbc.getDefaultConnection().close();
			} catch (SQLException e) {
				throw new LucyDataAccessException(systemConfig.getSystemName(), e);
			}			
		return rows;			
		}
		
		public static List<Map<String, String>> getListForTableInnerJoin(SystemConfig systemConfig,String fromTable,String joinTable, final List<String> columnNames) throws LucyDataAccessException {
			final List<Map<String, String>> rows = new ArrayList<Map<String, String>>();	
			DBContext dbc = DB.getDefaultContext(systemConfig.getPoolName());
		try {
			Select s = SQL.Select(dbc);
			s.from(fromTable);
			s.select(fromTable + ".content_id");
			s.select(fromTable + ".content_type_guid");
			s.select(fromTable + ".data");
			s.select(fromTable + ".title");			
			s.join(joinTable, fromTable+".content_id", joinTable+".content_id");

			final int numWantedColumns = columnNames.size();
				s.iterate(new ResultProcessor() {
					public void process(ResultSet rs) throws SQLException {					    
				     
				            Map<String, String> row = new LinkedHashMap<String, String>();
				 
				            for (int i = 0; i < numWantedColumns; ++i)
				            {
				                String columnName   = (String)columnNames.get(i);
				                String value = rs.getString(columnName);
				                row.put(columnName, value);
				            }			 
				            rows.add(row);					
					}				
				});
				dbc.getDefaultConnection().close();
			} catch (SQLException e) {
				throw new LucyDataAccessException(systemConfig.getSystemName(), e);	
			}	
			return rows;
		}
}


