package org.evolvis.lucy.dataaccess;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Marco Hellenthal tarent GmbH Bonn
 * 
 * This Class manages the keys for fetching the data from the database with
 */

public final class DataFieldNames {
	
	//Fieldnames for a typical Document in lucy	for the spezific system
	
	protected static final String bitweaverContentDataField = "data";
	protected static final String bitweaverContentTitleField = "title";
	protected static final String bitweaverContentIdField = "content_id";
	protected static final String bitweaverContentTypeField = "content_type_guid";
	protected static final String bitweaverlastModified = "last_modified";
	
	
	protected static final String bitweaverBlogDataField = "description";
	protected static final String bitweaverBlogTitleField = "title";
	protected static final String bitweaverBlogIdField = "blog_id";
	
	protected static final String bitweaverBlogPostDataField = "data";
	protected static final String bitweaverBlogPostTitleField = "title";
	protected static final String bitweaverBlogPostIdField = "content_id";
	protected static final String bitweaverBlogPostTypeField = "content_type_guid";	
	
	protected static final String mediawikiContentDataField = "si_text";
	protected static final String mediawikiContentTitleField = "page_title";
	protected static final String mediawikiContentTypeField = "page_namespace";
	protected static final String mediawikiContentIdField = "si_page";
	
	protected static final String gforgeContentDataField = "short_description";
	protected static final String gforgeContentUrlField = "homepage";
	protected static final String gforgeContentTitleField = "group_name";
	protected static final String gforgeContentIdField = "group_id";
	
	public final List<String> getColumnNamesForGForgeContent() {
		
		List<String> columnNames = new ArrayList<String>();
		columnNames.add(gforgeContentDataField);
		columnNames.add(gforgeContentUrlField);
		columnNames.add(gforgeContentTitleField);
		columnNames.add(gforgeContentIdField);	
		
		return columnNames;
	}
	
	public final List<String> getColumnNamesForBitweaverContent() {
		List<String> columnNames = new ArrayList<String>();
		columnNames.add(bitweaverContentDataField);
		columnNames.add(bitweaverContentTitleField);
		columnNames.add(bitweaverContentIdField);
		columnNames.add(bitweaverContentTypeField);
		columnNames.add(bitweaverlastModified);
		
		return columnNames;
	}
	
	public final List<String> getColumnNamesForBitweaverBlog() {
		List<String> columnNames = new ArrayList<String>();
		columnNames.add(bitweaverBlogDataField);
		columnNames.add(bitweaverBlogTitleField);
		columnNames.add(bitweaverBlogIdField);
		
		return columnNames;
	}
	public final List<String> getColumnNamesForBitweaverBlogPost() {
		List<String> columnNames = new ArrayList<String>();
		columnNames.add(bitweaverBlogPostDataField);
		columnNames.add(bitweaverBlogPostTitleField);
		columnNames.add(bitweaverBlogPostIdField);
		columnNames.add(bitweaverBlogPostTypeField);
		
		return columnNames;
	}
	
	public final List<String> getColumnNamesForMediaWikiContent() {
		
		List<String> columnNames = new ArrayList<String>();
		columnNames.add(mediawikiContentTitleField);
		columnNames.add(mediawikiContentTypeField);
		columnNames.add(mediawikiContentDataField);
		columnNames.add(mediawikiContentIdField);
		
		return columnNames;
	}
	
}