package org.evolvis.lucy.worker;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.jws.WebMethod;

import org.evolvis.lucy.config.IndexerConfig;
import org.evolvis.lucy.config.IndexerSystemConfig;
import org.evolvis.lucy.config.SystemConfig;
import org.evolvis.lucy.config.XmlConfigParser;
import org.evolvis.lucy.dataaccess.LucyDataAccessException;

import org.evolvis.lucy.hits.LucyHits;

import org.evolvis.lucy.indexer.LucyFactory;
import org.evolvis.lucy.indexer.LucyIndexProvider;
import org.evolvis.lucy.indexer.LucyRegistry;

import de.tarent.commons.logging.LogFactory;
import de.tarent.octopus.content.annotation.Name;
import de.tarent.octopus.content.annotation.Optional;
import de.tarent.octopus.content.annotation.Result;
import de.tarent.octopus.server.Context;
import de.tarent.octopus.server.OctopusContext;

public class IndexProviderWorker {
	
	@WebMethod()
	@Result("hits")
	public LucyHits query( @Name("q") @Optional(false) String q, @Name("s") @Optional(true) String s ) {
		
		File indexPath = null;
		LucyFactory fact = LucyRegistry.getInstance().getSystemByName(s);
		//If system is not in registry, we have to build its index and put it into
		if(fact == null){
			int size = getSystemsFromXMLConfigFile().size();
			for(int i = 0;size > i;i++){
				//Is the system which should be searched in the config?
				String tmp = getSystemsFromXMLConfigFile().get(i).getSystemName();
				if(tmp.equals(s)){
					indexPath = new File(getSystemsFromXMLConfigFile().get(i).getIndexPath());
					if(!indexPath.exists()){
						LucyFactory factoryToBuild = null;
						factoryToBuild = LucyFactory.create(getSystemsFromXMLConfigFile().get(i));
						if(factoryToBuild == null)
							return null;
						LogFactory.log("[lucy-indexer] Creating index for : " + factoryToBuild.getSystemConfig().getSystemName(), null);
						try {
							//Build factoryToBuild
							factoryToBuild.getIndexBuilder().rebuild();
							LucyRegistry.getInstance().put(s, LucyFactory.create(getSystemsFromXMLConfigFile().get(i)));
						} catch (LucyDataAccessException e) {
							LogFactory.log("[lucy-indexer] Rebuild for " + factoryToBuild.getSystemConfig().getSystemName() + " without success!", e);
						}
					}					
				}
				if(tmp == null)
					return null;
			}
		}
		//If system exists in registry, get the path to the index
		if(LucyRegistry.getInstance().getSystemByName(s) != null){
			indexPath = new File(LucyRegistry.getInstance().getSystemByName(s).getSystemConfig().getIndexPath());
		}
		//If System is indexed, but not allready in the registry because of a tomcat restart
		if(LucyRegistry.getInstance().getSystemByName(s) == null){
			List<SystemConfig> systems = getSystemsFromXMLConfigFile();
			for (SystemConfig systemConf : systems) {
				if(systemConf.getSystemName().equals(s) && new File(systemConf.getIndexPath()).exists())
					LucyRegistry.getInstance().put(s, LucyFactory.create(systemConf));
			}
		}
		//Fire query
		LucyHits hits = null;
		if(indexPath.exists()) {			
			LucyFactory system = LucyRegistry.getInstance().getSystemByName(s);
			LucyIndexProvider provider = system.getIndexProvider();
			hits = provider.executeQuery(q);
			return hits;
		} else {
			return null;
		}
	}
	/* This function rebuilds all systems located in the registry
	  */
	@WebMethod()	
	public void rebuild(OctopusContext oc) {
		List<String> list = LucyRegistry.getInstance().getNames();
		for(int i = 0;i<list.size();i++){
			//Get systems each system per name from registry
			LucyFactory system = LucyRegistry.getInstance().getSystemByName(list.get(i));
			SystemConfig systemConf = system.getSystemConfig();
			System.out.println("[lucy-indexer] Rebuilding... " + systemConf.getSystemName());
			try {
				system.getIndexBuilder().rebuild();
			} catch (LucyDataAccessException e) {
				LogFactory.log("[lucy-indexer] Rebuild for " + systemConf.getSystemName() + " without success!", e);
			}
			System.out.println("[lucy-indexer] Rebuild for " + systemConf.getSystemName() + " successfully finished!");
		}
	}
	//Initial indexingroutine, which indexes all given systems in the lucyConfig
	//Can be used from the lucy userinterface from the admin, to build first indexes with a click than a request
	@WebMethod()
	public void init() {
				
		List<SystemConfig> systems = getSystemsFromXMLConfigFile();	
		
		for (SystemConfig systemConf : systems) {
			try {
				File indexPath = new File(systemConf.getIndexPath());			
				if(!indexPath.exists()){
					LucyFactory lucyFactory = LucyFactory.create(systemConf);
					System.out.println("[lucy-indexer] Indexing : " + systemConf.getSystemName() + "...");
					lucyFactory.getIndexBuilder().rebuild();
					System.out.println("[lucy-indexer] Puting " + systemConf.getSystemName()+ " into registry.");
					LucyRegistry.getInstance().put(systemConf.getSystemName(), lucyFactory);
				} else {
					System.out.println("[lucy-indexer] " + systemConf.getSystemName() + " had allready been indexed.");
					System.out.println("[lucy-indexer] Puting " +  systemConf.getSystemName() + " into registry.");
					LucyFactory lucyFactory = LucyFactory.create(systemConf);
					LucyRegistry.getInstance().put(systemConf.getSystemName(), lucyFactory);
				}
			} catch (LucyDataAccessException e) {
				LogFactory.log("[lucy-indexer] Rebuild for " + systemConf.getSystemName() + " without success!", e);
			}
		}
	}
	@WebMethod
	@Result("systems")
	public ArrayList<String> getSystems(){
		Map<String, LucyFactory> systemList = LucyRegistry.getInstance().getSystems();
		if(systemList.size() == 0){
			init();
		}
		systemList = LucyRegistry.getInstance().getSystems();
		ArrayList<String> systems = new ArrayList<String>();
		Set<String> keys = systemList.keySet();
		for (String singleKey : keys) {
			systems.add(singleKey);
		}
		return systems;
	}
	
	public static List<SystemConfig> getSystemsFromXMLConfigFile() {
		XmlConfigParser cp = new XmlConfigParser();
		IndexerConfig config = new IndexerConfig();
		//Solution for a dependency between ui to indexer	
		//config = cp.deserialize((FileInputStream)WikiSearcher.class.getResourceAsStream("lucyconfig.xml"));
		try {				
			config = cp.deserialize(new FileInputStream(new File(Context.getActive().moduleRootPath().getParent(), "/lucyconfig.xml")));
		} catch (FileNotFoundException e) {
			LogFactory.log("[lucy-indexer] Could not get config file for path: " + Context.getActive().moduleRootPath().getParent()+"/lucyconfig.xml", e);
		}
		IndexerSystemConfig fact = config.getSystemFactory();
		List<SystemConfig> systems = fact.getSystems();
		
		return systems;
	
	}
}

